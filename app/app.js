/**
 * Created by felipe on 27/06/16.
 */
require('angular');
require('angular-route');
require('angular-sanitize');
var $ = jQuery = require("jquery");
//require('angular-datatables');
// var bootstrap = require('bootstrap');
var appConfig = require('./config/appConfig');
var HelperFactory = require('./factory/HelperFactory');
var LoginController = require('./controllers/LoginController');
var MainController = require('./controllers/MainController');
var GrupoController = require('./controllers/GrupoController');
var UsuarioController = require('./controllers/UsuariosController');
var SeguradoraController = require('./controllers/SeguradoraController');
var ProdutoController = require('./controllers/ProdutoController');
var CampanhaController = require('./controllers/CampanhaController');
var LeadController = require('./controllers/LeadController');
var FinanceiroController = require('./controllers/FinanceiroController');
var RedeCredenciadaController = require('./controllers/RedeCredenciadaController');
var GambicobController = require('./controllers/GambicobController');
//
var RoutesConfig = require('./config/RoutesConfig');

angular.module('app',['ngRoute','ngSanitize']);
angular.module('app').value('appConfig',appConfig);
angular.module('app').config(['$routeProvider',RoutesConfig]);
angular.module('app').factory('HelperFact',['$http','$location','appConfig',HelperFactory]);
angular.module('app').controller('MainCtrl',['$scope','$location','HelperFact',MainController]);
angular.module('app').controller('LoginCtrl',['$scope','$http','$location','appConfig','HelperFact',LoginController]);
angular.module('app').controller('GrupoCtrl',['$scope','$http','$location','appConfig','HelperFact',GrupoController]);
angular.module('app').controller('UsuarioCtrl',['$scope','$http','$location','appConfig','HelperFact',UsuarioController]);
angular.module('app').controller('SeguradoraCtrl',['$scope','$http','$location','appConfig','HelperFact',SeguradoraController]);
angular.module('app').controller('ProdutoCtrl',['$scope','$http','$location','appConfig','HelperFact',ProdutoController]);
angular.module('app').controller('CampanhaCtrl',['$scope','$http','$location','appConfig','HelperFact',CampanhaController]);
angular.module('app').controller('LeadCtrl',['$scope','$http','$location','$routeParams','appConfig','HelperFact',LeadController]);
angular.module('app').controller('FinanceiroCtrl',['$scope','$http','$location','$routeParams','appConfig','HelperFact',FinanceiroController]);
angular.module('app').controller('RedeCredenciadaCtrl',['$scope','$http','$location','$routeParams','appConfig','HelperFact',RedeCredenciadaController]);
angular.module('app').controller('GambicobCtrl',['$scope','$http','$location','$routeParams','appConfig','HelperFact',GambicobController]);


