/**
 * Created by felipemoura on 22/07/16.
 */
module.exports = function($http,$location,appConfig){

    var _modalAlert = function(message){
        html =
        ' <div id="myModal" class="modal fade" role="dialog">'+
            '<div class="modal-dialog">'+
                '<div class="modal-content">'+
                    '<div class="modal-header">'+
                        '<button type="button" class="close" data-dismiss="modal">&times;</button>'+
                        '<h4 class="modal-title">Alerta</h4>'+
                    '</div>'+
                    '<div class="modal-body">'+message+'</div>'+
                    '<div class="modal-footer">'+
                '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
                '</div>'+
                '</div>'+
            '</div>'+
        '</div>';
        jQuery('.modalAlerts').html(html);
        jQuery('#myModal').modal('toggle');
    };

    var _modalStruture = function(data,title){
        html =
            ' <div id="myModal" class="modal fade" role="dialog">'+
                '<div class="modal-dialog">'+
                    '<div class="modal-content">'+
                        '<div class="modal-header">'+
                            '<button type="button" class="close" data-dismiss="modal">&times;</button>'+
                            '<h4 class="modal-title">Alerta</h4>'+
                        '</div>'+
                        '<div class="modal-body">'+message+'</div>'+
                        '<div class="modal-footer">'+
                            '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>';
        jQuery('.modalAlerts').html(html);
        jQuery('#myModal').modal('toggle');
    };

    var _historyBack = function(){
        jQuery('.historyBack').click(function(e){
            e.preventDefault();
            window.history.back();
        });
    }

    var _alerts = function(message,type){
        html =
            '<div class="alert alert-success alert-dismissible fade in" role="alert">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">×</span></button>' + message + '</div>';
        if(type==="error"){
            html =
                '<div class="alert alert-danger alert-dismissible fade in" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">×</span></button>' + message + '</div>';
        }
        jQuery('.alerts').html(html);
        setTimeout(function () {
            jQuery('.alerts').empty();
        }, 2000);
    }

    var _getTokenValid = function(){
        var token = window.localStorage.getItem('token');
        $http({method:"get", url: appConfig.host+'/client/auth/getNewTokenByTokenValid',headers:{Authorization: "Bearer "+token}})
            .then(function(response){
                window.localStorage.setItem('token',response.data.token);
            },function(response){
                alert('Erro :'+response.data.message);
                window.localStorage.removeItem('token');
                window.localStorage.removeItem('userData');
                $location.path("/");
            });
    };

    var _checkTokenValidSession = function(message){
        var token = window.localStorage.getItem('token');
        if(message == "Token passado expirou" || message == "Token passado é invalido"){
            $location.path("/login");
        }
    };

    return {
        //btnReturn: _btnReturn,
        alerts: _alerts,
        historyBack: _historyBack,
        modalAlert: _modalAlert,
        getTokenValid: _getTokenValid,
        checkTokenValidSession: _checkTokenValidSession
    };
}
