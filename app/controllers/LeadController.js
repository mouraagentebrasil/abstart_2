/**
 * Created by felipemoura on 24/11/2016.
 */
module.exports = function ($scope, $http, $location, $routeParams, appConfig, HelperFact) {
    $scope.data = {};
    $scope.usuario = JSON.parse(window.localStorage.getItem('userData'));
    $scope.leads = {};
    $scope.leadsAgregados = {};
    $scope.input_search = null;
    $scope.params = $routeParams;
    $scope.lifeTimeLeads = {};
    $scope.leadContratacoes = {};
    $scope.contratacoes = {};
    $scope.html = "";
    $scope.dashboardLeads = {};
    $scope.produtos = {};
    $scope.usuarios = {};
    $scope.lead_agregado_a = {};

    $scope.dataI = null;
    $scope.dataF = null;
    //limpando o modal
    $('#myModal').empty();

    $scope.getLeads = function () {
        $.blockUI({'message':"Carregando..."});
        var token = window.localStorage.getItem('token');
        var url = appConfig.host + '/client/lead' + ($scope.input_search !== null ? "/" + $scope.input_search : "");
        $http({method: "get", url: url, headers: {Authorization: "Bearer " + token}})
            .then(function (response) {
                $.unblockUI();
                $scope.leads = response.data.data;
            }, function (response) {
                $.unblockUI();
                alert(response.data.message);
                ////console.log(response.data.message);
                HelperFact.checkTokenValidSession(response.data.message);
            });
        //regenera o token
        //var regenerateToken = HelperFact.getTokenValid();
    };

    $scope.leadEdit = function (lead) {
        $('#myModal').empty();
        html =
            ' <div id="myModal" class="modal fade" role="dialog">' +
            '<div class="modal-dialog modal-lg">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
            '<h4 class="modal-title">Editando Lead</h4>' +
            '</div>' +
            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Nome</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_nome" value="' + (lead.lead_nome === null ? "" : lead.lead_nome) + '">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Email</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_email" value="' + (lead.lead_email === null ? "" : lead.lead_email) + '">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Fone</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_fone" value="' + (lead.lead_fone === null ? "" : lead.lead_fone) + '">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead CPF</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_cpf" value="' + (lead.lead_cpf === null ? "" : lead.lead_cpf) + '">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Cep</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_cep" value="' + (lead.lead_cep === null ? "" : lead.lead_cep) + '">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead UF</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_uf" value="' + (lead.lead_uf === null ? "" : lead.lead_uf) + '">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Cidade</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_cidade" value="' + (lead.lead_cidade == null ? "" : lead.lead_cidade) + '">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Bairro</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_bairro" value="' + (lead.lead_bairro === null ? "" : lead.lead_bairro) + '">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Logradouro</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_logradouro" value="' + (lead.lead_logradouro === null ? "" : lead.lead_logradouro ) + '">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Complemento</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_complemento" value="' + (lead.lead_complemento === null ? "" : lead.lead_complemento ) + '">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Numero</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_numero" value="' + (lead.lead_numero === null ? "" : lead.lead_numero) + '">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Nascimento</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_nascimento" value="' + (lead.lead_nascimento === null ? "" : lead.lead_nascimento) + '">' +
            '</div>' +
            '</div>' +

            '<input type="hidden" value="' + lead.lead_id + '" id="lead_id">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Sexo</label>' +
            '<div class="col-sm-10">' +
            '<select id="lead_sexo" class="form-control">' +
            '<option value="m" ' + (lead.lead_sexo == "m" ? "selected" : "") + '>Masculino</option>' +
            '<option value="f" ' + (lead.lead_sexo == "f" ? "selected" : "") + '>Feminino</option>' +
            '</select>' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Cobrado</label>' +
            '<div class="col-sm-10">' +
            '<select id="lead_cobrado" class="form-control">' +
            '<option value="titular_pagador" ' + (lead.lead_cobrado === "titular_pagador" ? "selected" : "") + '>Titular Pagador</option>' +
            '<option value="pagador" ' + (lead.lead_cobrado === "pagador" ? "selected" : "") + '>Pagador</option>' +
            '</select>' +
            '</div>' +
            '</div>' +

            '</div></div>' +

            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>' +
            '<button type="button" class="btn btn-info btn-flat" id="atualizarLead">Atualizar</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        jQuery('.modalAlerts').html(html);
        jQuery('#myModal').modal('toggle');
        $('#lead_nascimento').datepicker({
            "useCurrent": true,
            "setDate": new Date(),
            "autoclose": true,
            "language": 'pt',
            "format": "yyyy-mm-dd"
        })
    };

    $scope.leadNew = function () {
        $('#myModal').empty();
        html =
            ' <div id="myModal" class="modal fade" role="dialog">' +
            '<div class="modal-dialog modal-lg">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
            '<h4 class="modal-title">Novo Lead</h4>' +
            '</div>' +
            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Nome</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_nome">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Email</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_email">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Fone</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_fone">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead CPF</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_cpf">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Cep</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_cep">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead UF</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_uf">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Cidade</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_cidade">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Bairro</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_bairro">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Logradouro</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_logradouro">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Complemento</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_complemento">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Numero</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_numero">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Nascimento</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_nascimento">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Lead Sexo</label>' +
            '<div class="col-sm-10">' +
            '<select id="lead_sexo" class="form-control">' +
            '<option value="m">Masculino</option>' +
            '<option value="f">Feminino</option>' +
            '</select>' +
            '</div>' +
            '</div>' +
            // '<div class="form-group">' +
            // '<label class="col-sm-2 control-label">Lead Cobrado</label>' +
            // '<div class="col-sm-10">' +
            // '<select id="lead_cobrado" class="form-control">' +
            // '<option value="titular_pagador">Titular Pagador</option>' +
            // '<option value="pagador">Pagador</option>' +
            // '</select>' +
            //'</div>' +
            //'</div>' +

            '</div></div>' +

            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>' +
            '<button type="button" class="btn btn-info btn-flat" id="newLead">Novo Lead</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        jQuery('.modalAlerts').html(html);
        jQuery('#myModal').modal('toggle');
        $('#lead_nascimento').datepicker({
            "useCurrent": true,
            "setDate": new Date(),
            "autoclose": true,
            "language": 'pt',
            "format": "yyyy-mm-dd"
        })
    };
    //event delegate evento de clique do modal de novo lead
    $(document).on('click','#newLead',function(e){
        e.preventDefault();
        $scope.data = {};
        $scope.data.lead_id = $('#lead_id').val();
        $scope.data.lead_nome = $('#lead_nome').val();
        $scope.data.lead_cpf = $('#lead_cpf').val();
        $scope.data.lead_cep = $('#lead_cep').val();
        $scope.data.lead_sexo = $('#lead_sexo').val();
        $scope.data.lead_uf = $('#lead_uf').val();
        $scope.data.lead_cidade = $('#lead_cidade').val();
        $scope.data.lead_bairro = $('#lead_bairro').val();
        $scope.data.lead_logradouro = $('#lead_logradouro').val();
        $scope.data.lead_numero = $('#lead_numero').val();
        $scope.data.lead_nascimento = $('#lead_nascimento').val();
        $scope.data.lead_fone = $('#lead_fone').val();
        $scope.data.lead_complemento = $('#lead_complemento').val();
        $scope.data.lead_email = $('#lead_email').val();
        $scope.data.lead_cobrado = $('#lead_cobrado').val();
        jQuery.debounce(250,$scope.newLead());
    });

    $scope.newLead = function(){
        var token = window.localStorage.getItem('token');
        $scope.data.usuario_id = JSON.parse(window.localStorage.getItem('userData')).usuario_id;
        $http({
            'method': 'post',
            'url': appConfig.host + '/client/lead/store',
            data: $scope.data,
            headers: {Authorization: token}
        })
        .then(function (response) {
                alert(response.data.message);
                $('#myModal').modal('hide');
                $('#myModal').empty();
            },
        function (response) {
            var message = response.data.message;
            if (Array.isArray(message)) {
                var html = 'Erros: \n';
                for (i = 0; i < response.data.message.length; i++) {
                    html += response.data.message[i] + '\n';
                }
                alert(html);
            } else {
                alert(response.data.message);
            }
        });
    };
    //event delegate evento de clique do modal de atualizar lead
    $(document).on('click', '#atualizarLead', function (e) {
        e.preventDefault();
        $scope.data = {};
        $scope.data.lead_id = $('#lead_id').val();
        $scope.data.lead_nome = $('#lead_nome').val();
        $scope.data.lead_cpf = $('#lead_cpf').val();
        $scope.data.lead_cep = $('#lead_cep').val();
        $scope.data.lead_sexo = $('#lead_sexo').val();
        $scope.data.lead_uf = $('#lead_uf').val();
        $scope.data.lead_cidade = $('#lead_cidade').val();
        $scope.data.lead_bairro = $('#lead_bairro').val();
        $scope.data.lead_logradouro = $('#lead_logradouro').val();
        $scope.data.lead_numero = $('#lead_numero').val();
        $scope.data.lead_nascimento = $('#lead_nascimento').val();
        $scope.data.lead_fone = $('#lead_fone').val();
        $scope.data.lead_complemento = $('#lead_complemento').val();
        $scope.data.lead_email = $('#lead_email').val();
        $scope.data.lead_cobrado = $('#lead_cobrado').val();
        jQuery.debounce(250,$scope.atualizarLead());
    });

    $scope.atualizarLead = function () {
        var token = window.localStorage.getItem('token');
        $scope.data.usuario_id = JSON.parse(window.localStorage.getItem('userData')).usuario_id;
        $http({
            'method': 'post',
            'url': appConfig.host + '/client/lead/update/' + $scope.data.lead_id,
            data: $scope.data,
            headers: {Authorization: token}
        })
            .then(function (response) {
                    alert(response.data.message);
                    $('#myModal').modal('hide');
                    $('#myModal').empty();
                },
                function (response) {
                    var message = response.data.message;
                    if (Array.isArray(message)) {
                        var html = 'Erros: \n';
                        for (i = 0; i < response.data.message.length; i++) {
                            html += response.data.message[i] + '\n';
                        }
                        alert(html);
                    } else {
                        alert(response.data.message);
                    }
                });
    };

    $scope.operacaoAgregarLead = function () {
        var token = window.localStorage.getItem('token');
        var url = appConfig.host + '/client/lead' + "/" + $scope.params.lead_id;
        $http({method: "get", url: url, headers: {Authorization: "Bearer " + token}})
            .then(function (response) {
                $scope.leads = response.data.data;

            }, function (response) {
                alert(response.data.message);
            });
        //regenera o token
        //var regenerateToken = HelperFact.getTokenValid();
    };

    $scope.back = function () {
        $scope.leads = {};
        //window.history.back(-1);
        $location.path('/lead/operacoes');
    };

    $scope.modalAddBeneficiario = function (lead) {
        $('#myModal').empty();
        html =
            '<div id="myModal" class="modal fade" role="dialog">' +
            '<div class="modal-dialog modal-lg">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
            '<h4 class="modal-title">Adicionando Titular Ou Beneficiario</h4>' +
            '</div>' +
            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Nome</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_nome">' +
            '</div>' +
            '</div>' +
            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Nome da Mãe</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_mae">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Email</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_email">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Fone</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_fone">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">CPF</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_cpf">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Cep</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_cep" value="'+lead.lead_cep+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">UF</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_uf" value="'+lead.lead_uf+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Cidade</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_cidade" value="'+lead.lead_cidade+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Bairro</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_bairro" value="'+lead.lead_bairro+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Logradouro</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_logradouro" value="'+lead.lead_logradouro+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Complemento</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_complemento" value="'+lead.lead_complemento+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Numero</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_numero" value="'+lead.lead_numero+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Nascimento</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_nascimento">' +
            '</div>' +
            '</div>' +

            //'<input type="hidden" data-value="' + JSON.stringify(lead) + '" id="lead_id">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Sexo</label>' +
            '<div class="col-sm-10">' +
            '<select id="lead_agregado_sexo" class="form-control">' +
            //'+ (lead.lead_sexo == "m" ? "selected" : "") +'
            '<option value="m">Masculino</option>' +
            '<option value="f">Feminino</option>' +
            '</select>' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Tipo</label>' +
            '<div class="col-sm-10">' +
            '<select id="lead_agregado_tipo" class="form-control">' +
            '<option value="beneficiario">Beneficiario</option>' +
            //' + (lead.lead_agregado_tipo === "beneficiario" ? "selected" : "") + '
            '<option value="titular">Titular</option>' +
            //' + (lead.lead_agregado_tipo === "titular" ? "selected" : "") + '
            '</select>' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Parentesco</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_parentesco">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Ativar/Inativar</label>' +
            '<div class="col-sm-10">' +
            '<select id="fl_ativo" class="form-control">'+
                '<option value="1">Ativo</option>'+
                '<option value="0">Inativo</option>'+
            '</select>'+
            '</div>' +
            '</div>' +
                '<input type="hidden" id="lead_id" value="'+lead.lead_id+'">'+
            '</div></div>' +

            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>' +
            '<button type="button" class="btn btn-info btn-flat" id="addLeadAgregado">Adicionar</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        jQuery('.modalAlerts').html(html);
        jQuery('#myModal').modal('toggle');
        $('#lead_agregado_nascimento').datepicker({
            "useCurrent": true,
            "setDate": new Date(),
            "autoclose": true,
            "language": 'pt',
            "format": "yyyy-mm-dd"
        });
    };

    //event delegate evento de clique do modal adicionar lead agregado
    $(document).on('click','#addLeadAgregado',function(e){
        $scope.data = {};
        $scope.data.lead_agregado_tipo = $('#lead_agregado_tipo').val();
        $scope.data.lead_agregado_nome = $('#lead_agregado_nome').val();
        $scope.data.lead_agregado_cpf = $('#lead_agregado_cpf').val();
        $scope.data.lead_agregado_nascimento = $('#lead_agregado_nascimento').val();
        $scope.data.lead_agregado_mae = $('#lead_agregado_mae').val();
        $scope.data.lead_agregado_email = $('#lead_agregado_email').val();
        $scope.data.lead_agregado_fone = $('#lead_agregado_fone').val();
        $scope.data.lead_agregado_cep = $('#lead_agregado_cep').val();
        $scope.data.lead_agregado_uf = $('#lead_agregado_uf').val();
        $scope.data.lead_agregado_cidade = $('#lead_agregado_cidade').val();
        $scope.data.lead_agregado_bairro = $('#lead_agregado_bairro').val();
        $scope.data.lead_agregado_logradouro = $('#lead_agregado_logradouro').val();
        $scope.data.lead_agregado_complemento = $('#lead_agregado_complemento').val();
        $scope.data.lead_agregado_numero = $('#lead_agregado_numero').val();
        $scope.data.lead_agregado_sexo = $('#lead_agregado_sexo').val();
        $scope.data.lead_agregado_parentesco = $('#lead_agregado_parentesco').val();
        $scope.data.fl_ativo = $('#fl_ativo').val();
        $scope.data.lead_id = $('#lead_id').val();
        jQuery.debounce(250,$scope.addLeadAgregado());
    });

    $scope.addLeadAgregado = function(){
        var token = window.localStorage.getItem('token');
        $scope.data.usuario_id = JSON.parse(window.localStorage.getItem('userData')).usuario_id;
        var url = appConfig.host + '/client/lead_agregado/store';
        $http({method: "post", url: url, data: $scope.data, headers: {Authorization: "Bearer " + token}})
            .then(function (response) {
                alert(response.data.message);
                $('#myModal').modal('hide');
                $('#myModal').empty();
                $scope.getLeadAgregado();
            }, function (response) {
                var message = response.data.message;
                if(Array.isArray(message)) {
                    var html = 'Erros: \n';
                    for (i = 0; i < response.data.message.length; i++) {
                        html += response.data.message[i] + '\n';
                    }
                    alert(html);
                }else {
                    alert(response.data.message);
                }
            });
    };

    $scope.modalEditBeneficiario = function (leadAgregado) {
        $('#myModal').empty();
        html =
            '<div id="myModal" class="modal fade" role="dialog">' +
            '<div class="modal-dialog modal-lg">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
            '<h4 class="modal-title">Editando Titular Ou Beneficiario</h4>' +
            '</div>' +
            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Nome</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_nome" value="'+(leadAgregado.lead_agregado_nome === null ? "" : leadAgregado.lead_agregado_nome)+'">' +
            '</div>' +
            '</div>' +
            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Nome da Mãe</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_mae" value="'+(leadAgregado.lead_agregado_mae === null ? "" : leadAgregado.lead_agregado_mae)+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Email</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_email" value="'+(leadAgregado.lead_agregado_email === null ? "": leadAgregado.lead_agregado_email)+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Fone</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_fone" value="'+(leadAgregado.lead_agregado_fone ===null ? "" : leadAgregado.lead_agregado_fone )+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">CPF</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_cpf" value="'+(leadAgregado.lead_agregado_cpf === null ? "" : leadAgregado.lead_agregado_cpf)+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Cep</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_cep" value="'+(leadAgregado.lead_agregado_cep === null ? "" : leadAgregado.lead_agregado_cep )+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">UF</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_uf" value="'+(leadAgregado.lead_agregado_uf === null ? "" : leadAgregado.lead_agregado_uf)+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Cidade</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_cidade" value="'+(leadAgregado.lead_agregado_cidade === null ? "" : leadAgregado.lead_agregado_cidade)+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Bairro</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_bairro" value="'+(leadAgregado.lead_agregado_bairro === null ? "" : leadAgregado.lead_agregado_bairro )+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Logradouro</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_logradouro" value="'+(leadAgregado.lead_agregado_logradouro === null ? "" : leadAgregado.lead_agregado_logradouro)+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Complemento</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_complemento" value="'+(leadAgregado.lead_agregado_complemento === null ? "": leadAgregado.lead_agregado_complemento)+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Numero</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_numero" value="'+(leadAgregado.lead_agregado_numero === null ? "" : leadAgregado.lead_agregado_numero)+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Nascimento</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_nascimento" value="'+(leadAgregado.lead_agregado_nascimento === null ? "" : leadAgregado.lead_agregado_nascimento)+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Sexo</label>' +
            '<div class="col-sm-10">' +
            '<select id="lead_agregado_sexo" class="form-control">' +
            //'+ (lead.lead_sexo == "m" ? "selected" : "") +'
            '<option value="m" '+ (leadAgregado.lead_agregado_sexo == "m" ? "selected" : "") +'>Masculino</option>' +
            '<option value="f" '+ (leadAgregado.lead_agregado_sexo == "f" ? "selected" : "") +'>Feminino</option>' +
            '</select>' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Tipo</label>' +
            '<div class="col-sm-10">' +
            '<select id="lead_agregado_tipo" class="form-control">' +
            '<option value="beneficiario">Beneficiario</option>' +
            '<option value="titular_pagador" ' + (leadAgregado.lead_agregado_tipo == "titular_pagador" ? "selected" : "") + '>Titular Pagador</option>' +
            '<option value="titular" ' + (leadAgregado.lead_agregado_tipo == "titular" || leadAgregado.lead_agregado_tipo == "titular_pagador" ? "selected" : "") + '>Titular</option>' +
            '<option value="beneficiario" ' + (leadAgregado.lead_agregado_tipo == "beneficiario" ? "selected" : "") + '>Beneficiario</option>' +
            '</select>' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Parentesco</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_parentesco" value="'+(leadAgregado.lead_agregado_parentesco === null ? "" : leadAgregado.lead_agregado_parentesco)+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Estado Civil</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_agregado_civil" value="'+(leadAgregado.lead_agregado_parentesco === null ? "" : leadAgregado.lead_agregado_civil)+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Ativar/Inativar</label>' +
            '<div class="col-sm-10">' +
            '<select id="fl_ativo" class="form-control">'+
            '<option value="1" '+(leadAgregado.fl_ativo == 1 ? "selected" : "")+'>Ativo</option>'+
            '<option value="0" '+(leadAgregado.fl_ativo == 0 ? "selected" : "")+'>Inativo</option>'+
            '</select>'+
            '</div>' +
            '</div>' +
            '<input type="hidden" id="lead_id" value="'+leadAgregado.lead_id+'">'+
            '<input type="hidden" id="lead_agregado_id" value="'+leadAgregado.lead_agregado_id+'">'+
            '</div></div>' +

            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>' +
            '<button type="button" class="btn btn-info btn-flat" id="atualizarLeadAgregado">Adicionar</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        jQuery('.modalAlerts').html(html);
        jQuery('#myModal').modal('toggle');
        $('#lead_agregado_nascimento').datepicker({
            "useCurrent": true,
            "setDate": new Date(),
            "autoclose": true,
            "language": 'pt',
            "format": "yyyy-mm-dd"
        });
    };

    //event delegate evento de clique do modal atualizar lead agregado
    $(document).on('click', '#atualizarLeadAgregado', function (e) {
        e.preventDefault();
        $scope.data = {};
        $scope.data.lead_agregado_tipo = $('#lead_agregado_tipo').val();
        $scope.data.lead_agregado_nome = $('#lead_agregado_nome').val();
        $scope.data.lead_agregado_cpf = $('#lead_agregado_cpf').val();
        $scope.data.lead_agregado_nascimento = $('#lead_agregado_nascimento').val();
        $scope.data.lead_agregado_mae = $('#lead_agregado_mae').val();
        $scope.data.lead_agregado_email = $('#lead_agregado_email').val();
        $scope.data.lead_agregado_fone = $('#lead_agregado_fone').val();
        $scope.data.lead_agregado_cep = $('#lead_agregado_cep').val();
        $scope.data.lead_agregado_uf = $('#lead_agregado_uf').val();
        $scope.data.lead_agregado_cidade = $('#lead_agregado_cidade').val();
        $scope.data.lead_agregado_bairro = $('#lead_agregado_bairro').val();
        $scope.data.lead_agregado_logradouro = $('#lead_agregado_logradouro').val();
        $scope.data.lead_agregado_complemento = $('#lead_agregado_complemento').val();
        $scope.data.lead_agregado_numero = $('#lead_agregado_numero').val();
        $scope.data.lead_agregado_sexo = $('#lead_agregado_sexo').val();
        $scope.data.fl_ativo = $('#fl_ativo').val();
        $scope.data.lead_id = $('#lead_id').val();
        $scope.data.lead_agregado_id = $('#lead_agregado_id').val();
        $scope.data.lead_agregado_parentesco = $('#lead_agregado_parentesco').val();
        jQuery.debounce(250,$scope.updateLeadAgregado());
    });

    //monta corrigir
    // $scope.listaContratacoes = function(){
    //     leadContratacoes = $scope.leadContratacoes;
    //     //console.log(leadContratacoes);
    //     $scope.html =
    //         '<div style="padding: 20px;">'+
    //         '<h3>Contratações</h3><hr><br>';
    //     for(i=0;i<leadContratacoes.length;i++) {
    //         $scope.html +=
    //             '<div style="overflow-y: scroll; height: 200px;">' +
    //             '<h3>' + leadContratacoes[i].produto.produto_nome +
    //             ' - Data Contratação: ' + leadContratacoes[i].financeiro_contratacao.data_contratacao +
    //             ' - Dia Recorrencia: ' + leadContratacoes[i].financeiro_contratacao.dia_recorrencia +
    //             ' - Valor Mensal: ' + leadContratacoes[i].financeiro_contratacao.valor_mensal +
    //             '</h3>' +
    //             '<table class="table table-striped">' +
    //             '<thead>' +
    //             '<tr>' +
    //             '<th>Forma de Pagamento/Gateway</th>' +
    //             '<th>Data Inclusão</th>' +
    //             '<th>Data Vencimento</th>' +
    //             '<th>Valor</th>' +
    //             '<th>Foi pago ?</th>' +
    //             '</tr>' +
    //             '</thead>' +
    //             '<tbody>';
    //         for (cobranca = 0; cobranca < leadContratacoes[i].financeiro_contratacao.financeiro_cobranca.length; cobranca++) {
    //             cobranca = leadContratacoes[i].financeiro_contratacao.financeiro_cobranca[cobranca];
    //             //console.log(cobranca);
    //             $scope.html += '<td>' + cobranca.gateway_slug + '</td>';
    //             $scope.html += '<td>' + cobranca.data_inclusao + '</td>';
    //             $scope.html += '<td>' + cobranca.data_vencimento + '</td>';
    //             $scope.html += '<td>' + cobranca.valor + '</td>';
    //             $scope.html += '<td>' + cobranca.foi_pago + '</td>';
    //         }
    //         $scope.html += "</tbody></table></div>";
    //     }
    //     $scope.html += "</div>";
    //     return $scope.html;
    // };

    $scope.getContratacoes = function(){
        var token = window.localStorage.getItem('token');
        $scope.data.usuario_id = JSON.parse(window.localStorage.getItem('userData')).usuario_id;
        //console.log($scope.leads);
        var url = appConfig.host + '/client/financeiro-ab/getContratacao/'+$scope.params.lead_id;
        $http({method: "get", url: url, data: $scope.data, headers: {Authorization: "Bearer " + token}})
            .then(function (response) {
                $scope.contratacoes = response.data;
                //console.log($scope.contratacoes);
                //console.log(response);
            },function (response) {
                alert(response.data.message);
                HelperFact.checkTokenValidSession(response.data.message);
            });
    };

    $scope.updateLeadAgregado = function(){
        var token = window.localStorage.getItem('token');
        $scope.data.usuario_id = JSON.parse(window.localStorage.getItem('userData')).usuario_id;
        var url = appConfig.host + '/client/lead_agregado/update/'+$scope.data.lead_agregado_id;
        $http({method: "post", url: url, data: $scope.data, headers: {Authorization: "Bearer " + token}})
            .then(function (response) {
                alert(response.data.message);
                $('#myModal').modal('hide');
                $('#myModal').empty();
                $scope.getLeadAgregado();
                $scope.data = {};
            }, function (response) {
                var message = response.data.message;
                if(Array.isArray(message)) {
                    var html = 'Erros: \n';
                    for (i = 0; i < response.data.message.length; i++) {
                        html += response.data.message[i] + '\n';
                    }
                    alert(html);
                }else {
                    alert(response.data.message);
                    HelperFact.checkTokenValidSession(response.data.message);
                }
            });
    };

    $scope.getLeadAgregado = function(){
        $.blockUI({'message':"Carregando..."});
        var token = window.localStorage.getItem('token');
        var url = appConfig.host + '/client/lead/getAggregatesByLead' + "/" + $scope.params.lead_id;
        $http({method: "get", url: url, headers: {Authorization: "Bearer " + token}})
            .then(function (response) {
                $scope.leadsAgregados = response.data;
                $.unblockUI();
            }, function (response) {
                $.unblockUI();
                alert(response.data.message);
            });
        //var regenerateToken = HelperFact.getTokenValid();
    };

    $scope.getLeadAgregadoContratacao = function(lead_agregado_id){
        var token = window.localStorage.getItem('token');
        var url = appConfig.host + '/client/lead_produto_agregado/getLeadAgregadoContratacao' + "/" + lead_agregado_id;
        $http({method: "get", url: url, headers: {Authorization: "Bearer " + token}})
            .then(function (response) {
                $scope.leadContratacoes = response.data;
            });
    };
    //corrigir
    $scope.visualizarLeadAgregado = function(leadAgregado){
        $('#myModal').empty();

        html =
            '<div id="myModal" class="modal fade" role="dialog">' +
            '<div class="modal-dialog modal-lg">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
            '<h4 class="modal-title">Visualizando Titular Ou Beneficiario</h4>' +
            '</div>' +
            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Nome: </label>' + leadAgregado.lead_agregado_nome +
            '</div>' +
            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Nome da Mãe: </label>' + leadAgregado.lead_agregado_mae +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Email: </label>' + leadAgregado.lead_agregado_email +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Fone: </label>' + leadAgregado.lead_agregado_fone +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">CPF: </label>' + leadAgregado.lead_agregado_cpf +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Cep: </label>' + leadAgregado.lead_agregado_cep +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">UF: </label>' + leadAgregado.lead_agregado_uf +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Cidade: </label>' + leadAgregado.lead_agregado_cidade +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Bairro: </label>' + leadAgregado.lead_agregado_bairro +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Logradouro: </label>' + leadAgregado.lead_agregado_logradouro +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Complemento: </label>' + leadAgregado.lead_agregado_complemento +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Numero: </label>' + leadAgregado.lead_agregado_numero +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Nascimento: </label>' + leadAgregado.lead_agregado_nascimento +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Sexo: </label>' + (leadAgregado.lead_agregado_sexo == "m" ? "Masculino" : "Feminino") +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Tipo: </label>' + (leadAgregado.lead_agregado_tipo === "titular" ? "Titular" : "Beneficiario") +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Status: </label> ' + (leadAgregado.fl_ativo == 1 ? "Ativo" : "Inativo") +
            '</div>' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Parentesco: </label>' + leadAgregado.lead_agregado_parentesco +
            '</div>' +
            '</div>' +
            '</div>' +
            //$scope.listaContratacoes() +
            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        jQuery('.modalAlerts').html(html);
        jQuery('#myModal').modal('toggle');
    };

    $scope.destroyLeadAgregado = function(leadAgregado){
        $scope.confirm = confirm("Você tem certeza que quer deletar o "+leadAgregado.lead_agregado_tipo+" "+leadAgregado.lead_agregado_nome+" ?");
        if($scope.confirm == true){
            var token = window.localStorage.getItem('token');
            var url = appConfig.host + '/client/lead_agregado/destroy' + "/" + leadAgregado.lead_agregado_id;
            $http({method: "get", url: url, headers: {Authorization: "Bearer " + token}})
                .then(function (response) {
                    alert(response.data.message);
                    $scope.getLeadAgregado();
                }, function (response) {
                    alert(response.data.message);
                    HelperFact.checkTokenValidSession(response.data.message);
                });
        }
    };

    $scope.montaTable = function(data){
        var html = '';
        html += '<tbody>';
        for(i=0;i<data.length;i++){
            html+= '<tr>';
            html+= ' <th></th>';
            html+= ' <td>Acesso Total</td>';
            html+= ' <td>Acessos Unicos</td>';
            html+= ' <td>Numero de Convertidos</td>';
            html+= ' </tr>';
            html+= '<tr>';
            html+= ' <th>'+data[i].data_archiv+'</th>';
            html+= ' <td>'+data[i].hits_totais+'</td>';
            html+= ' <td>'+data[i].hits_unicos+'</td>';
            html+= ' <td>'+data[i].total_conversao+'</td>';
            html+= ' </tr>';
        }
        html += '</tbody>';
        $('#datatable').html(html);
    };

    $scope.setTableGraph = function(){
        url = appConfig.hostStats+'/stats/graphs/acessRange';
        url = $scope.dataI === null ? url : url+'/'+$scope.dataI;
        url = ($scope.dataF  === null || $scope.dataF  == '') ? url : url+'/'+$scope.dataF;
        $http({method: "get", url: url})
            .then(function (response) {
                $scope.montaTable(response.data);
                Highcharts.chart('container', {
                    data: {
                        table: 'datatable'
                    },
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Acessos e Conversão'
                    },
                    yAxis: {
                        allowDecimals: false,
                        title: {
                            text: 'Units'
                        }
                    }
                });
                //console.log(response);
            }, function (response) {
                alert(response.data.message);
                HelperFact.checkTokenValidSession(response.data.message);
            });
    };

    $scope.verCob = function(cobranca_id){
        var url = 'https://pag.agentebrasil.com/pagamentos/get-cobranca-by-fcobid/'+cobranca_id;

        $http({'method':'get', 'url':url })
            .then(function(response){
                $.unblockUI();
                //console.log(response);
                prompt("Link para o Boleto: ","https://pag.agentebrasil.com/pagamentos/vb/"+response.data.cobranca_id);
                //$scope.cobrancas = response;
            },function(response){
                $.unblockUI();
                //console.log(response);
                HelperFact.checkTokenValidSession(response.data.message);
            });
    };

    $scope.init = function(){
        $scope.setTableGraph();
    };

    $scope.getGraph = function(){
        $scope.dataI = $('#dateI').val();
        $scope.dataF = $('#dateF').val();
        $scope.init();
    };

    $scope.getTimeLifeLead = function(){
        var token = window.localStorage.getItem('token');
        var url = appConfig.host+'/client/lead_life_time/'+$scope.params.lead_id;
        $http({method: "get", url: url, headers: {Authorization: "Bearer " + token}})
            .then(function(response){
                $scope.lifeTimeLeads = response.data;
            },function(response){
                alert(response.data.message);
                HelperFact.checkTokenValidSession(response.data.message);
            });
    };

    $scope.dashboardGetLeadByRange = function() {
        var token = window.localStorage.getItem('token');
        var dateI = $('#dateI').val();
        var dateF = $('#dateF').val();
        var url = appConfig.host+'/client/get-leads-by-range/'+dateI+"/"+dateF;
        $http({method: "get", url: url, headers: {Authorization: "Bearer " + token}})
            .then(function(response){
                    $scope.dashboardLeads = response.data;
                    //console.log($scope.dashboardLeads);
                    $('#listagem_leads').removeClass("show_off");
                },
                function(response){
                    alert(response.data.message);
                    HelperFact.checkTokenValidSession(response.data.message);
                });
    };

    $scope.desativaPlano = function (objectContract) {
        userData = JSON.parse(window.localStorage.getItem('userData'));
        var reason = prompt("Qual o motivo do cancelamento ?");
        if(reason!=null){
            $.blockUI({'message':"Carregando..."});
            $http({
                    method: "get", url: 'https://client.agentebrasil.com/client/desativa-plano/'+objectContract.financeiro_contratacao_id+'/'+userData.usuario_id+'/'+reason,
                })
                .then(function (response) {
                    $.unblockUI();
                    alert('Cancelado!');
                    $scope.getContratacoes();
                },function (response) {
                    $.unblockUI();
                    alert(response.data.message);
                });
        }
    };

    $scope.viewPlano = function(objectContract){
        var html = "Contratados: \n\n";
        for(var i = 0; i < objectContract.lead_produto_agregado_itens.length; i++){
            html += objectContract.lead_produto_agregado_itens[i].lead_agregado.lead_agregado_id+" - "+objectContract.lead_produto_agregado_itens[i].lead_agregado.lead_agregado_nome+'\n';
        }
        alert(html);
    };

    $scope.updateContract = function(objectContract,forma_pagamento){
        $('#myModal').empty();
        userData = JSON.parse(window.localStorage.getItem('userData'));
        //console.log(objectContract)
        if(forma_pagamento==1){
            $.blockUI({message:"Atualizando..."});
            $scope.processUpdate(objectContract.financeiro_contratacao_id,forma_pagamento,userData.usuario_id)
            return;
        }
        html =
            '<div id="myModal" class="modal fade" role="dialog">' +

            '<div class="modal-dialog modal-lg">' +

                    '<div class="modal-content">' +
                        '<div class="modal-header">' +
                            '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
                            '<h4 class="modal-title">Atualizar Contratação para cartão de crédito</h4>' +
                        '</div>' +

                        '<div class="modal-body">' +
                            '<div class="row">' +
                                '<div class="form-group">' +
                                    '<label class="col-sm-2 control-label">Numero do Cartão de Crédito </label>' +
                                    '<div class="col-sm-10">' +
                                        '<input type="text" class="form-control" id="lead_card_numero">' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +

                        '<div class="modal-body">' +
                            '<div class="row">' +
                                '<div class="form-group">' +
                                    '<label class="col-sm-2 control-label">Nome no Cartão</label>' +
                                    '<div class="col-sm-10">' +
                                        '<input type="text" class="form-control" id="lead_card_titular_nome">' +
                                    '</div>' +
                                '</div>'+
                            '</div>' +
                        '</div>' +

                        '<div class="modal-body">' +
                            '<div class="row">' +
                                '<div class="form-group">' +
                                    '<label class="col-sm-2 control-label">Ano do Cartão</label>' +
                                    '<div class="col-sm-10">' +
                                        '<input type="text" class="form-control" id="lead_card_ano">' +
                                    '</div>' +
                                '</div>'+
                            '</div>' +
                        '</div>' +

                        '<div class="modal-body">' +
                            '<div class="row">' +
                                '<div class="form-group">' +
                                    '<label class="col-sm-2 control-label">Mês do Cartão</label>' +
                                    '<div class="col-sm-10">' +
                                        '<input type="text" class="form-control" id="lead_card_mes">' +
                                    '</div>' +
                                '</div>'+
                            '</div>' +
                        '</div>' +

                        '<div class="modal-body">' +
                            '<div class="row">' +
                                '<div class="form-group">' +
                                    '<label class="col-sm-2 control-label">Bandeira do Cartão</label>' +
                                    '<div class="col-sm-10">' +
                                        '<input type="text" class="form-control" id="lead_card_bandeira">' +
                                    '</div>' +
                                '</div>'+
                            '</div>' +
                        '</div>' +

                        '<div class="modal-body">' +
                            '<div class="row">' +
                                '<div class="form-group">' +
                                    '<label class="col-sm-2 control-label">Código de Segurança</label>' +
                                    '<div class="col-sm-10">' +
                                        '<input type="text" class="form-control" id="lead_card_cvv">' +
                                    '</div>' +
                                '</div>'+
                            '</div>' +
                        '</div>' +

                        '<input type="hidden" class="form-control" id="usuario_id" value="'+userData.usuario_id+'">' +
                        '<input type="hidden" class="form-control" id="financeiro_contratacao_id" value="'+objectContract.financeiro_contratacao_id+'">' +
                        '<input type="hidden" class="form-control" id="financeiro_forma_pagamento_id" value="'+forma_pagamento+'">' +

                        '<div class="modal-footer">' +
                            '<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>' +
                            '<button type="button" class="btn btn-info btn-flat" id="updateContract">Atualizar</button>' +
                        '</div>' +

                    '</div>' +

                '</div>' +

            '</div>';
        jQuery('.modalAlerts').html(html);
        jQuery('#myModal').modal('toggle');
    };

    $(document).on('click','#updateContract',function(){

        $.blockUI({'message':"Atualizando informações aguarde..."});
        forma_pagamento = $('#financeiro_forma_pagamento_id').val();
        financeiro_contracao_id = $('#financeiro_contratacao_id').val();
        if(forma_pagamento==2){
            var objectSend = {
                'lead_card_cvv': $('#lead_card_cvv').val(),
                'lead_card_numero': $('#lead_card_numero').val(),
                'lead_card_titular_nome': $('#lead_card_titular_nome').val(),
                'lead_card_mes': $('#lead_card_mes').val(),
                'lead_card_ano': $('#lead_card_ano').val(),
                'lead_card_bandeira': $('#lead_card_bandeira').val(),
                'usuario_id':$('#usuario_id').val(),
                'financeiro_contratacao_id':financeiro_contracao_id,
                'financeiro_forma_pagamento_id':forma_pagamento
            };
        }
        $('#myModal').modal('hide');
        $('#myModal').empty();
        var url = "https://client.agentebrasil.com/client/update-contract/"+financeiro_contracao_id;
        $http({ method: "post", url: url, data: objectSend })
            .then(function(response){
                    $.unblockUI();
                    alert('Atualizado!');
                },
                function(response){
                    $.unblockUI();
                    alert(response.data.message);
                    HelperFact.checkTokenValidSession(response.data.message);
                });
    });

    $scope.processUpdate = function(financeiro_contratacao_id,forma_pagamento,usuario_id){
        var url = "https://client.agentebrasil.com/client/update-contract/"+financeiro_contratacao_id;
        var objectSend = {
            'usuario_id':usuario_id,
            'financeiro_forma_pagamento_id':forma_pagamento
        };
        $http({ method: "post", url: url, data: objectSend })
            .then(function(response){
                    $.unblockUI();
                    alert('Atualizado!');
                },
                function(response){
                    $.unblockUI();
                    alert(response.data.message);
                    HelperFact.checkTokenValidSession(response.data.message);
                });
    };

    $scope.processUpdateCob = function(financeiro_cobranca_id,forma_pagamento,usuario_id){
        var url = "https://client.agentebrasil.com/client/update-cobranca/"+financeiro_cobranca_id;
        var objectSend = {
            'usuario_id':usuario_id,
            'financeiro_forma_pagamento_id':forma_pagamento,
            'lead_id':$scope.params.lead_id
        };
        $http({ method: "post", url: url, data: objectSend })
            .then(function(response){
                    $.unblockUI();
                    alert('Atualizado!');
                },
                function(response){
                    $.unblockUI();
                    alert(response.data.message);
                    HelperFact.checkTokenValidSession(response.data.message);
                });
    };

    $scope.updateCobrancaBoleto = function (cobranca) {
        $('#myModal').empty();
        userData = JSON.parse(window.localStorage.getItem('userData'));
        //console.log(cobranca)
        $.blockUI({'message':"Atualizando informações aguarde..."});
        $scope.processUpdateCob(cobranca.financeiro_cobranca_id, 1, userData.usuario_id)
        return;
    };

    $scope.updateCobrancaCartao = function (cobranca) {
        $('#myModal').empty();
        userData = JSON.parse(window.localStorage.getItem('userData'));
        html =
            '<div id="myModal" class="modal fade" role="dialog">' +

            '<div class="modal-dialog modal-lg">' +

            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
            '<h4 class="modal-title">Atualizar Contratação para cartão de crédito</h4>' +
            '</div>' +

            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Numero do Cartão de Crédito </label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_card_numero">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +

            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Nome no Cartão</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_card_titular_nome">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +

            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Ano do Cartão</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_card_ano">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +

            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Mês do Cartão</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_card_mes">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +

            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Bandeira do Cartão</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_card_bandeira">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +

            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Código de Segurança</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="lead_card_cvv">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +


            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Data de Vencimento</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" class="form-control" id="data_vencimento">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +

            '<input type="hidden" class="form-control" id="usuario_id" value="' + userData.usuario_id + '">' +
            '<input type="hidden" class="form-control" id="lead_id" value="' + $scope.params.lead_id + '">' +
            '<input type="hidden" class="form-control" id="financeiro_cobranca_id" value="' + cobranca.financeiro_cobranca_id + '">' +
            '<input type="hidden" class="form-control" id="financeiro_forma_pagamento_id" value="2">' +

            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>' +
            '<button type="button" class="btn btn-info btn-flat" id="updateCobranca">Atualizar</button>' +
            '</div>' +

            '</div>' +

            '</div>' +

            '</div>';
        jQuery('.modalAlerts').html(html);
        $('#data_vencimento').datepicker({
            "useCurrent": true,
            "setDate": new Date(),
            "autoclose": true,
            "language": 'pt',
            "format": "yyyy-mm-dd"
        });
        jQuery('#myModal').modal('toggle');
    };

    $scope.getUsuarios = function(){
        var token = window.localStorage.getItem('token');
        var url = appConfig.host + '/client/usuario';
        $http({method: "get", url: url, headers: {Authorization: "Bearer " + token}})
            .then(function (response) {
                $scope.usuarios = response.data;
                ////console.log($scope.usuarios);
            },function (response) {
                alert(response.data.message);
                HelperFact.checkTokenValidSession(response.data.message);
            });
    };

    $scope.getProdutos = function(){
        var token = window.localStorage.getItem('token');
        var url = appConfig.host+'/client/produto'+($scope.input_search !== null ? "/"+$scope.input_search : "");
        $http({'method':'get','url':url,'async':false,
            'cache': false, headers:{Authorization: token }})
            .then(function (response) {
                ////console.log(response.data);
                $scope.produtos = response.data;
            },function(response){
                alert(response.data.message);
                HelperFact.checkTokenValidSession(response.data.message);
            });
       // var regenerateToken = HelperFact.getTokenValid();
    };

    $scope.getAgregadoAvailable = function(){
        var token = window.localStorage.getItem('token');
        var url = appConfig.host+'/client/get-leads-available/'+$scope.params.lead_id;
        $http({'method':'get','url':url})
            .then(function (response) {
                ////console.log(response);
                $scope.lead_agregado_a = response.data;
            },function(response){
                //alert('Sem');
                HelperFact.checkTokenValidSession(response.data.message);
            });
        // var regenerateToken = HelperFact.getTokenValid();
    };

    $scope.novaContratacao = function(lead) {
        if($scope.lead_agregado_a.length==0){
            alert("Sem beneficiarios para novas contratações!");
            return;
        }
        $('#myModal').empty();
        html =
            '<div id="myModal" class="modal fade" role="dialog">' +
            '<div class="modal-dialog modal-lg">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
            '<h4 class="modal-title">Nova Contratação</h4>' +
            '</div>' +

            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Produto: </label>' +
            '<div class="col-sm-10">' +
            '<select name="" id="produto_id" class="form-control">' ;
                for(i=$scope.produtos.length-1;i>0;i--){
                    html += '<option value="'+$scope.produtos[i].produto_id+'">'+$scope.produtos[i].produto_nome+" | "+$scope.produtos[i].seguradora.seguradora_nome+'</option>';
                }
            html += '</select>'+
            '</div>'+
            '</div>'+
            '</div>' +
            '</div>' +

            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Vendedor: </label>' +
            '<div class="col-sm-10">' +
            '<select name="" id="usuario_id" class="form-control">' ;
                for(i=$scope.usuarios.length-1;i>0;i--){
                    html += '<option value="'+$scope.usuarios[i].usuario_id+'">'+($scope.usuarios[i].nome==null ? $scope.usuarios[i].usuario_id+' - '+$scope.usuarios[i].email : $scope.usuarios[i].usuario_id+' - '+$scope.usuarios[i].nome)+'</option>';
                }
            html += '</select>'+
            '</div>'+
            '</div>'+
            '</div>' +
            '</div>' +

            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Forma Pagamento: </label>' +
            '<div class="col-sm-10">' +
            '<select name="" id="financeiro_forma_pagamento_id" class="form-control">' +
                '<option value="1">Boleto|AgenteBrasil</option>' +
                //'<option value="2">Cartão de Credito</option>' +
            '</select>'+
            '</div>'+
            '</div>'+
            '</div>' +
            '</div>' +

            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Data Vencimento</label>' +
            '<div class="col-sm-10">' +
                '<input type="text" class="form-control" id="data_vencimento">' +
            '</div>' +
            '</div>'+
            '</div>' +
            '</div>' +

            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Dia Recorrêcia</label>' +
            '<div class="col-sm-10">' +
                '<input type="text" class="form-control" id="dia_recorrencia">' +
            '</div>' +
            '</div>'+
            '</div>' +
            '</div>' +

            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Beneficiarios: </label>' +
            '<div class="col-sm-10">' +

            '<div class="checkbox">';

            for(i=0;i<$scope.lead_agregado_a.length;i++){
                html+='<label class="checkbox-inline">';
                html += '<input type="checkbox" name="lead_agregado_itens[]" value="'+$scope.lead_agregado_a[i].lead_agregado_id+'">'+$scope.lead_agregado_a[i].lead_agregado_nome;
                html += '</label>';
            }
            html+='</div>'+

            '</div>'+
            '</div>'+
            '</div>' +
            '</div>' +

            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>' +
            '<button type="button" class="btn btn-info btn-flat" id="newContract">Adicionar</button>' +
            '</div>' +

            '</div>' +
            '</div>' +
            '</div>';
        jQuery('.modalAlerts').html(html);
        jQuery('#myModal').modal('toggle');
        $('#data_vencimento').datepicker({
            "useCurrent": true,
            "setDate": new Date(),
            "autoclose": true,
            "language": 'pt',
            "format": "yyyy-mm-dd"
        });
    };

    $scope.getStatusPedidoMetlife = function(fcob){
        var url = appConfig.host+'/client/check-contract-metlife/'+fcob.financeiro_contratacao_id;
        $http({'method':'get','url':url})
            .then(function (response) {
                if(response.data[0].obs=="PROCESSADO." || response.data[0].obs=="QUANTIDADE MAXIMA DE ACUMULO DE RISCO ULTRAPASSADA."  ){
                    $('#rCM').html("<span class='btn btn-success btn-xs btn-flat'>PROCESSADO "+response.data[0].data_processado+".</span>");
                } else {
                    $('#rCM').html("<span class='btn btn-primary btn-xs btn-flat'>Ainda não processado</span>");
                }
            },function(response){
                $('#rCM').html("<span class='btn btn-primary btn-xs btn-flat'>Ainda não processado</span>");
            });
    };


    $scope.verificarIntegracoes = function(financeiro_contratacao){
        $('#myModal').empty();
        $.blockUI({'message':"Atualizando informações aguarde..."});
        //console.log(objectContract);
        var url = appConfig.host+'/client/get-all-integracoes/'+financeiro_contratacao.financeiro_contratacao_id;
        $http({'method':'get','url':url})
            .then(function (response) {
                $.unblockUI();
                console.log(response)
                html =
                    '<div id="myModal" class="modal fade" role="dialog">' +
                    '<div class="modal-dialog modal-lg">' +
                    '<div class="modal-content">' +
                    '<div class="modal-header">' +
                    '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
                    '<h4 class="modal-title">Contratacao: '+financeiro_contratacao.financeiro_contratacao_id+'</h4>' +
                    '</div>' +

                    '<div class="modal-body">' +
                    '<div class="row">' +
                    '<div class="form-group">' +
                    '<div class="col-sm-10">';
                var p_cpf = 0;
                var is_proc = true;
                for(var i = 0; i < response.data.length; i++){
                    if(response.data[i].obs=="PROCESSADO." || response.data[i].obs=="QUANTIDADE MAXIMA DE ACUMULO DE RISCO ULTRAPASSADA.") {
                        is_proc = false;
                    }
                    html += 'Arquivo: <b>'+response.data[i].remessa_nome.split('.')[0]+
                        '</b> - Observação:  <b>'+(response.data[i].obs === "" ? "NADA" : response.data[i].obs)+
                        '</b> - Data da Tentativa: <b>'+response.data[i].remessa_data+'</b>' +
                        '</b> - Data da Processado: <b>'+(response.data[i].data_processado === "" ? "AINDA NÃO PROCESSADO" : response.data[i].data_processado)+'</b><br>' +
                        '<a target="_blank" href="https://client.agentebrasil.com/client/view-remessa/'+response.data[i].remessa_nome.split('.')[0]+'">link log remessa integração</a> | '+
                        '<a target="_blank" href="https://client.agentebrasil.com/client/view-retorno/'+response.data[i].remessa_nome.split('.')[0]+'">link log retorno integração</a>'+
                        '<br><br>';
                }
                if(is_proc===true){
                    html+='<a target="_blank" class="btn btn-primary" href="https://client.agentebrasil.com/client/generateRemByContract/'+financeiro_contratacao.financeiro_contratacao_id+'">Forçar Integração</a>';
                }
                html +=
                    '</div>'+
                    '</div>'+
                    '</div>' +
                    '</div>'+
                    '<div class="modal-footer">' +
                    '<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>' +
                    // '<button type="button" class="btn btn-info btn-flat" id="newContract">Adicionar</button>' +
                    '</div>' +

                    '</div>' +
                    '</div>' +
                    '</div>';
                jQuery('.modalAlerts').html(html);
                jQuery('#myModal').modal('toggle');
                $('#data_vencimento').datepicker({
                    "useCurrent": true,
                    "setDate": new Date(),
                    "autoclose": true,
                    "language": 'pt',
                    "format": "yyyy-mm-dd"
                });
            },function(response){
                $.unblockUI();
            })

    };

    $(document).on('click','#newContract',function(){
        var items = [];
       //$.blockUI({'message':"Atualizando informações aguarde..."});
        $("input[name='lead_agregado_itens[]']:checked").each(function(){items.push($(this).val());});
        var object = {
            'lead_agregado_produto': {
                'usuario_id':$('#usuario_id').val(),
                'produto_id':$('#produto_id').val()
            },
            'financeiro_contratacao': {
                'data_vencimento':$('#data_vencimento').val(),
                'dia_recorrencia':$('#dia_recorrencia').val(),
                'financeiro_forma_pagamento_id':$('#financeiro_forma_pagamento_id').val(),
                'pedido_sac':1
            },
            'lead_agregado_produto_itens':{
                'lead_agregado_id':items
            }
        };
        if($('#data_vencimento').val() === "" || $('#dia_recorrencia').val()===""){
            alert('Preencha a data de vencimento e dia da recorrência!');
            return;
        }
        if(items.length===0){
            alert('Escolha pelo menos um beneficiario!');
            return;
        }

        $('#myModal').modal('hide');
        $('#myModal').empty();
        var url = 'https://client.agentebrasil.com/client/lead_produto_agregado2/venda_produto';
        $http({ method: "post", url: url, data: object })
            .then(function(response){
                    $.unblockUI();
                    alert('Salvo com sucesso!');
                },
                function(response){
                    $.unblockUI();
                    alert(response.data.message);
                    HelperFact.checkTokenValidSession(response.data.message);
                });
    });

    $(document).on('click','#updateCobranca',function(){

        $.blockUI({'message':"Atualizando informações aguarde..."});
        forma_pagamento = $('#financeiro_forma_pagamento_id').val();
        financeiro_cobranca_id = $('#financeiro_cobranca_id').val();
        if(forma_pagamento==2){
            var objectSend = {
                'lead_card_cvv': $('#lead_card_cvv').val(),
                'lead_card_numero': $('#lead_card_numero').val(),
                'lead_card_titular_nome': $('#lead_card_titular_nome').val(),
                'lead_card_mes': $('#lead_card_mes').val(),
                'lead_card_ano': $('#lead_card_ano').val(),
                'lead_card_bandeira': $('#lead_card_bandeira').val(),
                'usuario_id':$('#usuario_id').val(),
                'financeiro_cobranca_id':financeiro_cobranca_id,
                'financeiro_forma_pagamento_id':forma_pagamento,
                'lead_id': $('#lead_id').val(),
                'data_vencimento': $('#data_vencimento').val()
            };
        }
        $('#myModal').modal('hide');

        var url = "https://client.agentebrasil.com/client/update-cobranca/"+financeiro_cobranca_id;
        $http({ method: "post", url: url, data: objectSend })
            .then(function(response){
                    $.unblockUI();
                    alert('Atualizado!');
                    $('#myModal').empty();
                },
                function(response){
                    $('#myModal').empty();
                    $.unblockUI();
                    alert(response.data.message);
                    HelperFact.checkTokenValidSession(response.data.message);
                });
    });

    $(document).on('click','#updateCobrancaCard',function(){

        $.blockUI({'message':"Atualizando informações aguarde..."});
        forma_pagamento = $('#financeiro_forma_pagamento_id').val();
        financeiro_cobranca_id = $('#financeiro_cobranca_id').val();
        if(forma_pagamento==2){
            var objectSend = {
                'lead_card_cvv': $('#lead_card_cvv').val(),
                'lead_card_numero': $('#lead_card_numero').val(),
                'lead_card_titular_nome': $('#lead_card_titular_nome').val(),
                'lead_card_mes': $('#lead_card_mes').val(),
                'lead_card_ano': $('#lead_card_ano').val(),
                'lead_card_bandeira': $('#lead_card_bandeira').val(),
                'usuario_id':$('#usuario_id').val(),
                'financeiro_cobranca_id':financeiro_contracao_id,
                'financeiro_forma_pagamento_id':forma_pagamento,
                'lead_id': $('#lead_id').val()
            };
        }
        $('#myModal').modal('hide');
        $('#myModal').empty();
        var url = "https://client.agentebrasil.com/client/update-cobranca/"+financeiro_cobranca_id;
        $http({ method: "post", url: url, data: objectSend })
            .then(function(response){
                    $.unblockUI();
                    alert('Atualizado!');
                },
                function(response){
                    $.unblockUI();
                    alert(response.data.message);
                    HelperFact.checkTokenValidSession(response.data.message);
                });
    });

    $('#dateI').datepicker({
        "useCurrent": true,
        "setDate": new Date(),
        "autoclose": true,
        "language": 'pt',
        "format": "yyyy-mm-dd"
    });

    $('#dateF').datepicker({
        "useCurrent": true,
        "setDate": new Date(),
        "autoclose": true,
        "language": 'pt',
        "format": "yyyy-mm-dd"
    });

    $('#searchBar').keyup(function(e){
        if(e.which === 13){
            $scope.getLeads();
        }
    });

    $scope.atualizaBoleto = function (cob_id,lead_id) {
        var userData = JSON.parse(window.localStorage.getItem('userData'));
        var today = new Date();
        var date = prompt("Digite a data corretamente", today.getDate()+"/"+("00"+(today.getMonth()+1)).slice(-2)+"/"+today.getFullYear());
        $scope.data.usuario_id = JSON.parse(window.localStorage.getItem('userData')).usuario_id;
        if(date != null && $scope.maintainMask(date)) {
            $.blockUI({'message':"Atualizando informações aguarde..."});
            var url = appConfig.host+'/client/atualiza-boleto-with-log/'+cob_id+'/'+$scope.formatDate(date)+'/'+lead_id+'/'+$scope.data.usuario_id;
            $http({'method':'get','url':url})
                .then(function (response) {
                    $.unblockUI();
                    alert('Boleto atualizado para a data: '+date);
                },function(response){
                    $.unblockUI();
                   alert(response);
                });
        } else if(date == null) {
            //console.log('atualização cancelado');
        }
        else {
            $scope.atualizaBoleto();
        }
    };

    $scope.atualizaCartao = function (cob_id,lead_id) {
        var userData = JSON.parse(window.localStorage.getItem('userData'));
        var today = new Date();
        var date = prompt("Digite a data corretamente", today.getDate()+"/"+("00"+(today.getMonth()+1)).slice(-2)+"/"+today.getFullYear());
        $scope.data.usuario_id = JSON.parse(window.localStorage.getItem('userData')).usuario_id;
        if(date != null && $scope.maintainMask(date)) {
            $.blockUI({'message':"Atualizando informações aguarde..."});
            var url = appConfig.host+'/client/atualiza-boleto-with-log/'+cob_id+'/'+$scope.formatDate(date)+'/'+lead_id+'/'+$scope.data.usuario_id;
            $http({'method':'get','url':url})
                .then(function (response) {
                    $.unblockUI();
                    alert('Boleto atualizado para a data: '+date);
                },function(response){
                    $.unblockUI();
                    alert(response);
                });
        } else if(date == null) {
            //console.log('atualização cancelado');
        }
        else {
            $scope.atualizaBoleto();
        }
    };

    $scope.maintainMask = function(date){
        var dia = date.split("/")[0];
        var mes = date.split("/")[1];
        var ano = date.split("/")[2];
        if(dia.length == 2 && mes.length== 2 && ano.length ==4){
            return true;
        }
        return false;
    };

    $scope.formatDate = function (date){
        return date.split("/")[2]+"-"+date.split("/")[1]+"-"+date.split("/")[0];
    };

    $scope.requestBoletoAvulso = function (lead) {
        $('#myModal').empty();
        html =
            '<div id="myModal" class="modal fade" role="dialog">' +
            '<div class="modal-dialog modal-lg">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
            '<h4 class="modal-title">Gerando Cobranca Avulsa</h4>' +
            '</div>' +
            '<div class="modal-body">' +
                '<div class="row">' +
                    '<div class="form-group">' +
                        '<label class="col-sm-2 control-label">Nome</label>' +
                        '<div class="col-sm-10">' +
                            lead.lead_nome+ " - cpf: " + lead.lead_cpf +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div class="row">' +
                    '<div class="form-group">' +
                        '<label class="col-sm-2 control-label">Valor</label>' +
                        '<div class="col-sm-10">' +
                            '<div class="input-group">'+
                            '<div class="input-group-addon">R$</div>'+
                                "<input type='text' class='form-control' id='boleto_valor' style='width: 120px;'>"+
                            '</div>'+
                        '</div>' +
                    '</div>' +
                '</div>'+
                '<div class="row" style="margin-top: 25px;">' +
                    '<div class="form-group">' +
                        '<label class="col-sm-2 control-label">Data de Vencimento Boleto</label>' +
                        '<div class="col-sm-10">' +
                            '<input type="text" class="form-control" id="vencimento_boleto" style="width: 120px;">'+
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div class="row" style="margin-top: 25px;">' +
                    '<div class="form-group">' +
                        '<label class="col-sm-2 control-label">Instruções</label>' +
                        '<div class="col-sm-10">' +
                            '<textarea class="form-control" id="instrucoes_boleto" placeholder="Separe as instruções por virgula, ex: Referente a multa de quebra de contrato, Cobrar juros de de 2%, "></textarea>'+
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<input type="hidden" value="' + lead.lead_id + '" id="lead_id">' +
            '<div class="modal-footer">' +
                '<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>' +
                '<button type="button" class="btn btn-info btn-flat" id="gerarBoleto">Gerar Boleto</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        $('#boleto_valor').mask('000.000.000.000.000,00', {reverse: true});
        jQuery('.modalAlerts').html(html);
        jQuery('#myModal').modal('toggle');
        $('#vencimento_boleto').datepicker({
            "useCurrent": true,
            "setDate": new Date(),
            "autoclose": true,
            "language": 'pt-BR',
            "format": "yyyy-mm-dd"
        })
    };

    $scope.enviarBoletoSMS = function (cob_id, lead_id) {
        $scope.confirm = confirm("Deseja enviar EMAIL e SMS ?");
        if($scope.confirm == true){
            $scope.usuario_id = JSON.parse(window.localStorage.getItem('userData')).usuario_id;
            $.blockUI({'message':"Enviando boleto para o email e sms cadastrados do cliente (lead_id)..."});
            var url = appConfig.host+'/client/send-email-sms-cob/'+cob_id+'/'+lead_id.lead_id+'/'+$scope.usuario_id;
            $http({'method':'get','url':url})
                .then(function (response) {
                    $.unblockUI();
                    alert('Enviado!');
                },function(response){
                    $.unblockUI();
                    alert(response);
                });
        }
    };

    $scope.viewContratacao = function(objectContract){
        $('#myModal').empty();
        //console.log(objectContract);
        html =
            '<div id="myModal" class="modal fade" role="dialog">' +
            '<div class="modal-dialog modal-lg">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
            '<h4 class="modal-title">Contratacao: '+objectContract.financeiro_contratacao[0].financeiro_contratacao_id+'</h4>' +
            '</div>' +

            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<div class="col-sm-10">';
        var p_cpf = 0;

        for(var i = 0; i < objectContract.lead_produto_agregado_itens.length; i++){
            html +=
                'LeadAgregadoID: <b>'+objectContract.lead_produto_agregado_itens[i].lead_agregado.lead_agregado_id+'</b>'
                +" - Nome: <b>"+objectContract.lead_produto_agregado_itens[i].lead_agregado.lead_agregado_nome+'</b>'+
                ' - CPF: <b>'+
                ((objectContract.lead_produto_agregado_itens[i].lead_agregado.lead_agregado_cpf.trim() === "") ?
                    "SEM CPF" :
                    objectContract.lead_produto_agregado_itens[i].lead_agregado.lead_agregado_cpf);

            // if(objectContract.lead_produto_agregado_itens[i].lead_agregado.lead_agregado_tipo=="beneficiario" &&
            //     objectContract.lead_produto_agregado_itens[i].lead_agregado.lead_agregado_cpf.trim() !== ""){
            //     html +=' - <a class="btn btn-danger btn-xs btn-flat" ' +
            //         ' data-lead_agregado_id="'+objectContract.lead_produto_agregado_itens[i].lead_agregado.lead_agregado_id+
            //         '" id="cancelarBeneficiarioUnico">CANCELAR BENEFICIARIO</a>'+
            //         '</b><br>';
            // } else {
            //     html += '</b> - Tipo: '+objectContract.lead_produto_agregado_itens[i].lead_agregado.lead_agregado_tipo+'<br>';
            // }
            html += '</b> - Tipo: '+objectContract.lead_produto_agregado_itens[i].lead_agregado.lead_agregado_tipo+'<br>';
        }
        html +=
            '</div>'+
            '</div>'+
            '</div>' +
            '</div>'+
            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>' +
            // '<button type="button" class="btn btn-info btn-flat" id="newContract">Adicionar</button>' +
            '</div>' +

            '</div>' +
            '</div>' +
            '</div>';
        jQuery('.modalAlerts').html(html);
        jQuery('#myModal').modal('toggle');
        $('#data_vencimento').datepicker({
            "useCurrent": true,
            "setDate": new Date(),
            "autoclose": true,
            "language": 'pt',
            "format": "yyyy-mm-dd"
        });
    };

    $scope.enviarAutoPilot = function(financeiro_cobranca_id, financeiro_contratacao_id){
        $('#myModal').empty();
        //console.log(objectContract);
        html =
            '<div id="myModal" class="modal fade" role="dialog">' +
            '<div class="modal-dialog modal-lg">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
            '<h4 class="modal-title">Contratacao: '+financeiro_contratacao_id+'</h4>' +
            '</div>' +

            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="form-group">' +
            '<div class="col-sm-10">';
        html += 'COBRANCA: <br>';
        html+= '<a class="btn btn-warning btn-xs btn-flat enviaAutopilot" data-cob="'+financeiro_cobranca_id+'" data-fc_id="'+financeiro_contratacao_id+'" data-contact_id="contactlist_A4EA9A77-0210-44C5-BEA8-95B23F8F8E85">Enviar COBRANÇA METLIFE 1 dia </a><br>';
        html+= '<a class="btn btn-warning btn-xs btn-flat enviaAutopilot" data-cob="'+financeiro_cobranca_id+'" data-fc_id="'+financeiro_contratacao_id+'" data-contact_id="contactlist_6E5F7C80-19DB-4B05-B7AB-ABBFE7CBB3D1">Enviar COBRANÇA METLIFE 2 dia </a><br>';
        html+= '<a class="btn btn-warning btn-xs btn-flat enviaAutopilot" data-cob="'+financeiro_cobranca_id+'" data-fc_id="'+financeiro_contratacao_id+'" data-contact_id="contactlist_D1386039-8DA4-44BF-9285-BE496B05BC42">Enviar COBRANÇA METLIFE 3 dia </a><br>';
        html+= '<a class="btn btn-warning btn-xs btn-flat enviaAutopilot" data-cob="'+financeiro_cobranca_id+'" data-fc_id="'+financeiro_contratacao_id+'" data-contact_id="contactlist_C70420CA-4241-40D4-918B-DD96B13759D1">Enviar COBRANÇA METLIFE 4 dia </a><br>';
        html+= '<a class="btn btn-warning btn-xs btn-flat enviaAutopilot" data-cob="'+financeiro_cobranca_id+'" data-fc_id="'+financeiro_contratacao_id+'" data-contact_id="contactlist_192C332B-320A-4F0C-80F4-E90EDEC3BAD7">Enviar COBRANÇA METLIFE 5 dia </a><br>';
        html+= '<a class="btn btn-warning btn-xs btn-flat enviaAutopilot" data-cob="'+financeiro_cobranca_id+'" data-fc_id="'+financeiro_contratacao_id+'" data-contact_id="contactlist_F7C61A85-5C02-43DF-A805-9B241BD53599">Enviar COBRANÇA METLIFE 6 dia </a><br>';
        html+= '<a class="btn btn-warning btn-xs btn-flat enviaAutopilot" data-cob="'+financeiro_cobranca_id+'" data-fc_id="'+financeiro_contratacao_id+'" data-contact_id="contactlist_D0003F2A-9BF8-4D3B-A540-1AA586D77E13">Enviar COBRANÇA METLIFE 7 dia </a><br>';

        html+= '<br>REVENDA: <br>';

        html+= '<a class="btn btn-warning btn-xs btn-flat enviaAutopilot" data-cob="'+financeiro_cobranca_id+'" data-fc_id="'+financeiro_contratacao_id+'" data-contact_id="contactlist_AB8A99E9-FA1D-4803-ABA4-303D976C7F85">Enviar REVENDA METLIFE 1 dia </a><br>';
        html+= '<a class="btn btn-warning btn-xs btn-flat enviaAutopilot" data-cob="'+financeiro_cobranca_id+'" data-fc_id="'+financeiro_contratacao_id+'" data-contact_id="contactlist_506C5239-EB09-441B-83EB-D2AF80F1D739">Enviar REVENDA METLIFE 2 dia </a><br>';
        html+= '<a class="btn btn-warning btn-xs btn-flat enviaAutopilot" data-cob="'+financeiro_cobranca_id+'" data-fc_id="'+financeiro_contratacao_id+'" data-contact_id="contactlist_7AF40C12-1B34-4A4B-8C1D-4DCFCC9D941C">Enviar REVENDA METLIFE 3 dia </a><br>';
        html+= '<a class="btn btn-warning btn-xs btn-flat enviaAutopilot" data-cob="'+financeiro_cobranca_id+'" data-fc_id="'+financeiro_contratacao_id+'" data-contact_id="contactlist_A6DD9000-71E1-4BEA-831A-F5C6DE2BF045">Enviar REVENDA METLIFE 4 dia </a><br>';
        html+= '<a class="btn btn-warning btn-xs btn-flat enviaAutopilot" data-cob="'+financeiro_cobranca_id+'" data-fc_id="'+financeiro_contratacao_id+'" data-contact_id="contactlist_E161D4AB-1AF6-46B0-A7D0-7B72BF509DE5">Enviar REVENDA METLIFE 5 dia </a><br>';
        html+= '<a class="btn btn-warning btn-xs btn-flat enviaAutopilot" data-cob="'+financeiro_cobranca_id+'" data-fc_id="'+financeiro_contratacao_id+'" data-contact_id="contactlist_E34DC7B7-8730-41BC-BDB1-6101A5B57FCC">Enviar REVENDA METLIFE 6 dia </a><br>';
        html+= '<a class="btn btn-warning btn-xs btn-flat enviaAutopilot" data-cob="'+financeiro_cobranca_id+'" data-fc_id="'+financeiro_contratacao_id+'" data-contact_id="contactlist_8A129EAC-DEB7-431D-8E9B-D71681A2A6A0">Enviar REVENDA METLIFE 7 dia </a><br>';

        html +=
            '</div>'+
            '</div>'+
            '</div>' +
            '</div>'+
            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>' +
            // '<button type="button" class="btn btn-info btn-flat" id="newContract">Adicionar</button>' +
            '</div>' +

            '</div>' +
            '</div>' +
            '</div>';
        jQuery('.modalAlerts').html(html);
        jQuery('#myModal').modal('toggle');
        $('#data_vencimento').datepicker({
            "useCurrent": true,
            "setDate": new Date(),
            "autoclose": true,
            "language": 'pt',
            "format": "yyyy-mm-dd"
        });
    };


    function enviarParaAutoPilot(financeiro_cobranca_id,financeiro_contratacao_id,contact_id){
        console.log(financeiro_contratacao_id)
        $scope.usuario_id = JSON.parse(window.localStorage.getItem('userData')).usuario_id;
        $.blockUI({'message':"Enviando boleto para o email e sms cadastrados do cliente (lead_id)..."});
        var url = appConfig.host+'/client/autopilot-update-cob/'+financeiro_contratacao_id+'/'+financeiro_cobranca_id+'/'+contact_id+'/'+$scope.usuario_id;
        $http({'method':'get','url':url})
            .then(function (response) {
                $.unblockUI();
                alert('Enviado!');
            },function(response){
                $.unblockUI();
                alert(response.message);
            });
    };

    $(document).on('click','#cancelarBeneficiarioUnico',function(){
        var la_id = $(this).data('lead_agregado_id');
        //TODO: atualizar o plano
        alert("LA_ID:"+la_id);
    });

    $(document).on('click','.enviaAutopilot',function (e) {
        e.preventDefault()
        var fc_cob = $(this).data('cob');
        var fc_id = $(this).data('fc_id');
        var contact_id = $(this).data('contact_id');
        jQuery.debounce(250,enviarParaAutoPilot(fc_cob,fc_id,contact_id));
    });

    $(document).on('click','#gerarBoleto',function(){
        var obj = {
            'lead_id':$('#lead_id').val(),
            'valor':$('#boleto_valor').val(),
            'data_vencimento':$('#vencimento_boleto').val(),
            'instrucoes_boleto':$('#instrucoes_boleto').val()
        };
        console.log(obj);
    });
    $scope.getProdutos();
    $scope.getUsuarios();
    $scope.getAgregadoAvailable();
};