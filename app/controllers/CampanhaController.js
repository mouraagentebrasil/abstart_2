/**
 * Created by felipemoura on 23/11/2016.
 */
module.exports = function($scope,$http,$location,appConfig, HelperFact){
    $scope.data = null;
    $scope.usuario = JSON.parse(window.localStorage.getItem('userData'));
    $scope.campanhas = {};
    $scope.input_search = null;

    $scope.cadastrarCampanha = function () {
        var token = window.localStorage.getItem('token');
        $scope.data.produto_id = $("#produto_id").val();
        $scope.data.usuario_id = $scope.usuario.usuario_id;
        $http({'method':'post','url':appConfig.host+'/client/campanha/store', data: $scope.data ,headers:{Authorization: token }})
            .then(function (response) {
                console.log(response.data.message);
                alert(response.data.message);
            },function(response){
                var message = response.data.message;
                if(Array.isArray(message)){
                    var html = 'Erros: \n';
                    for(i=0;i<response.data.message.length;i++){
                        html += response.data.message[i]+'\n';
                    }
                    alert(html);
                } else {
                    alert(response.data.message);
                    HelperFact.checkTokenValidSession(response.data.message);
                }

            });
        //var regenerateToken = HelperFact.getTokenValid();
    };

    $scope.getCampanhas = function(){
        var token = window.localStorage.getItem('token');
        var url = appConfig.host+'/client/campanha'+($scope.input_search !== null ? "/"+$scope.input_search : "");
        $http({method: "get", url: url, headers: {Authorization: "Bearer " + token}})
            .then(function (response) {
                console.log(response.data.data);
                $scope.campanhas = response.data.data;
                console.log($scope.campanhas);
            },function (response) {
                alert(response.data.message);
                HelperFact.checkTokenValidSession(response.data.message);
            });
        //regenera o token
        //var regenerateToken = HelperFact.getTokenValid();
    };

    $scope.getLeadsFB = function(campanha_name){
        $.blockUI({'message':"Processando..."});
        var token = window.localStorage.getItem('token');
        var url = appConfig.host+'/client/getLeadGenForms/'+campanha_name;
        $http({method: "get", url: url })
            .then(function (response) {
                $.unblockUI();
                console.log(response.data);
                alert(
                    '\n Sem rede credenciada: '+response.data.sem_redecredenciada+
                    '\n Incosistentes: '+response.data.incosistentes+
                    '\n Duplicados: '+response.data.duplicados+
                    '\n Disponiveis: '+response.data.disponiveis+
                    '\n Total: '+response.data.total
                );
            },function (response) {
                $.unblockUI();
                alert(response.data.message);
                HelperFact.checkTokenValidSession(response.data.message);
            });
    };
    //$('#dataTables').dataTable();
};