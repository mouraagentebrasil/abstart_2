/**
 * Created by felipemoura on 04/08/16.
 */
module.exports = function ($scope,$location,HelperFact) {
    HelperFact.checkTokenValidSession();
    $scope.infoUser = null;
    $scope.currentController = null;
    $scope.menus = null;
    $scope.init = function () {
        if(typeof (window.localStorage.getItem('userData')) !== 'undefined'){
            $scope.infoUser = JSON.parse(window.localStorage.getItem('userData'));
        }
        //$scope.activeCurrentUrl();
    };

    function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }

    function montaMenu(){
        $scope.menus = JSON.parse(window.localStorage.getItem('menus'));
        $scope.treeMenu = [];
        $scope.tree = [];

        for(k=0;k<$scope.menus.length;k++){
            value = $scope.menus[k];
            viewTree = value.rota_frontend.rota_frontend_url.split('#/')[1].split('/')[0];
            $scope.treeMenu[k] = viewTree;
        }
        $scope.treeMenu = $scope.treeMenu.filter(onlyUnique);
        $scope.tree='<li class="header">MAIN NAVIGATION</li>';

        for(k=0;k<$scope.treeMenu.length;k++){
            cat = $scope.treeMenu[k];
            $scope.tree+=
                '<li class="treeview" style="text-transform:capitalize;">'+
                '<a href="javascript;;">'+
                '<i class="fa fa-circle-o"></i><span>'+cat+'</span>'+
                '<span class="pull-right-container">'+
                '<i class="fa fa-angle-left pull-right"></i>'+
                '</span>'+
                '</a>'+
                '<ul class="treeview-menu">';
            for(j=0;j<$scope.menus.length;j++){
                value = $scope.menus[j];
                viewTree = value.rota_frontend.rota_frontend_url.split('#/')[1].split('/')[0];
                if(cat===viewTree){
                    icon = (value.rota_frontend.rota_frontend_icon === null)? "fa fa-circle-o" : value.rota_frontend.rota_frontend_icon;
                    $scope.tree += "<li><a href='"+value.rota_frontend.rota_frontend_url+"'><i class='"+icon+"'></i>"+value.rota_frontend.rota_frontend_nome+"</a></li>";
                }
            }
            $scope.tree+=
                '</ul></li>';
        }
        jQuery('.sidebar-menu').html($scope.tree);
    }

    $(document).ready(function(){
        montaMenu();
    });
    // $scope.activeCurrentUrl = function () {
    //     $scope.url = $location.absUrl().split('/');
    //     $scope.currentController = $scope.url[4];
    //     itens = $('.menus');
    //     itens.each(function(e){
    //         item = $(this).find('a');
    //
    //         if(item.attr('href').split('#/')[1] === $scope.currentController){
    //             $(this).addClass('active');
    //         }
    //     });
    // }
};