/**
 * Created by felipemoura on 20/01/2017.
 */
module.exports = function ($scope, $http, $location, $routeParams, appConfig, HelperFact) {
    $scope.data = {};
    $scope.seguradoras = {};
    $scope.seguradora_id = null;
    $scope.redeCredenciadas = {};
    $scope.uf = null;
    $scope.cidade = null;

    $scope.getSeguradoras = function(){
        var token = window.localStorage.getItem('token');
        $http({'method':'get','url':appConfig.host+'/client/seguradora', headers:{Authorization: token }})
            .then(function(response){
                $scope.seguradoras = response.data.data;
            },function(response){
                alert(response.data.message);
                HelperFact.checkTokenValidSession(response.data.message);
            });
    };

    $scope.getRedeCredenciada = function(){
        console.log(appConfig);
        var token = window.localStorage.getItem('token');
        var url = appConfig.host+'/client/rede_credenciada';
        url += $scope.seguradora_id !== null || $scope.seguradora_id == "" ? "/"+$scope.seguradora_id : "";
        url += $scope.uf !==  null || $scope.uf == "" ? "/"+$scope.uf : "";
        url += $scope.uf !== null && $scope.cidade !== null ? "/"+$scope.cidade : "";
        console.log(url);
        $http({'method':'get','url': url, headers:{Authorization: token }})
            .then(function(response){
                console.log(response);
                $scope.redeCredenciadas = response.data;
            },function(response){
                alert(response.data.message);
                HelperFact.checkTokenValidSession(response.data.message);
            });
    };
};