/**
 * Created by felipemoura on 21/11/2016.
 */
module.exports = function($scope,$http,$location,appConfig, HelperFact) {
    $scope.grupos = null;
    $scope.rotas = null;
    $scope.data = null;
    $scope.rotasFront = null;

    $('#myModal').empty();

    $scope.getGrupos = function () {
        var token = window.localStorage.getItem('token');
        $http({method: "get", url: appConfig.host + '/client/grupo', headers: {Authorization: "Bearer " + token}})
            .then(function (response) {
                $scope.grupos = response.data;
            },function(response){
                HelperFact.checkTokenValidSession(response.data.message);
            });
        //regenera o token
        //var regenerateToken = HelperFact.getTokenValid();
    };

    $scope.getRoute = function () {
        var token = window.localStorage.getItem('token');
        $http({method: "get", url: appConfig.host + '/client/rota', headers: {Authorization: "Bearer " + token}})
            .then(function (response) {
                $scope.rotas = response.data;
            },function(response){
                alert('Erro: '+response.data.message);
                HelperFact.checkTokenValidSession(response.data.message);
            });
        //regenera o token
        //var regenerateToken = HelperFact.getTokenValid();
    };

    $scope.cadastroGrupo = function (data) {
        var token = window.localStorage.getItem('token');
        data.rotas = $('#rotas_api').val();
        data.rotas_frontend = $('#rotas_front').val();

        $http({
            method: "post", url: appConfig.host + '/client/grupo/storeGrupoRota',
            headers: {Authorization: "Bearer " + token},
            data: data
        }).then(function (response) {
            alert(response.data.message);
            $scope.data = null;
        },function (response) {
            alert('Erro: '+response.data.message);
            HelperFact.checkTokenValidSession(response.data.message);
        });
        //regenera o token
        //var regenerateToken = HelperFact.getTokenValid();
    };

    $scope.getRouteFrontEnd = function () {
        var token = window.localStorage.getItem('token');
        $http({method: "get", url: appConfig.host + '/client/rota_frontend', headers: {Authorization: "Bearer " + token}})
            .then(function (response) {
                $scope.rotasFront = response.data;
            },function(response){
                alert('Erro: '+response.data.message);
                HelperFact.checkTokenValidSession(response.data.message);
            });
        //regenera o token
        //var regenerateToken = HelperFact.getTokenValid();
    };

    $scope.modalEditGrupo = function (grupo) {
        $('#myModal').empty();
        html =
            '<div id="myModal" class="modal fade" role="dialog">' +
            '<div class="modal-dialog modal-lg">' +
                '<div class="modal-content">' +
                    '<div class="modal-header">' +
                        '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
                        '<h4 class="modal-title">Alteração de Permissão</h4>' +
                    '</div>' +
                    '<div class="modal-body">' +
                        '<div class="row">' +
                            '<div class="form-group">' +
                                '<label class="col-sm-2 control-label">Grupo Nome</label>' +
                                '<div class="col-sm-10">' +
                                    '<input type="text" class="form-control" id="grupo_nome" style="width: 200px;" value="'+grupo.grupo_nome+'">' +
                                '</div>' +
                            '</div>' +
                            $scope.formatRouteApi(grupo) +
                            $scope.formatRouteFrontend(grupo)+
                        '</div>' +
                    '</div>'+
                    '<input type="hidden" value="'+grupo.grupo_id+'" id="grupo_id">'+
                    '<input type="hidden" value="'+JSON.stringify(grupo)+'" id="grupo">'+
                    '<div class="modal-footer">' +
                        '<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>' +
                        '<button type="button" class="btn btn-success btn-flat" id="updateGrupo">Atualizar</button>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '</div>';
        jQuery('.modalAlerts').html(html);
        jQuery('#myModal').modal('toggle');
        $("#rotas_api, #rotas_front").select2({
            tags: true
        });
    };

    $scope.formatRouteApi = function(grupo){

        function inArray(needle, haystack) {
            var length = haystack.length;
            for(var i = 0; i < length; i++) {
                if(haystack[i].rota_id == needle) return true;
            }
            return false;
        }

        html = '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Rotas API</label>' +
            '<div class="col-sm-10">';
        html += '<select class="form-control" id="rotas_api" multiple style="width: 400px;">';
        rotas = grupo.grupo_rota;
        for(i=0;i<$scope.rotas.length;i++){
            selected = inArray($scope.rotas[i].rota_id,rotas) ? "selected" : "";
            html+='<option value="'+$scope.rotas[i].rota_id+'" '+selected+'>'+$scope.rotas[i].descricao+'</option>';
        }
        html += "</select>";
        html += '</div></div>';
        return html;
    };

    $scope.formatRouteFrontend = function(grupo){

        function inArray(needle, haystack) {
            var length = haystack.length;
            for(var i = 0; i < length; i++) {
                if(haystack[i].rota_frontend_id == needle) return true;
            }
            return false;
        }

        html = '<div class="form-group" style="margin-top: 120px;">' +
            '<label class="col-sm-2 control-label">Rotas Front-End</label>' +
            '<div class="col-sm-10">';
        html += '<select class="form-control" id="rotas_front" multiple style="width: 400px;">';
        rotas = grupo.grupo_rota_frontend;
        for(i=0;i<$scope.rotasFront.length;i++){
            selected = inArray($scope.rotasFront[i].rota_frontend_id,rotas) ? "selected" : "";
            html+='<option value="'+$scope.rotasFront[i].rota_frontend_id+'" '+selected+'>'+$scope.rotasFront[i].rota_frontend_nome+'</option>';
        }
        html += "</select>";
        html += '</div></div>';
        return html;
    };

    $(document).on('click','#updateGrupo',function (e) {
        e.preventDefault();
        data = [];
        data = {
            'grupo_nome': $('#grupo_nome').val(),
            'grupo_id': $('#grupo_id').val(),
            'rota': $('#rotas_api').val(),
            'rota_frontend': $('#rotas_front').val()
        };
        console.log(data);

        var token = window.localStorage.getItem('token');
        jQuery.debounce(250,
            $http({method: "post", url: appConfig.host + '/client/grupo/updateGrupoRota', headers: {Authorization: "Bearer " + token}, data: data})
            .then(function (response) {
                alert(response.data.message);
                $scope.getGrupos();
                $('#myModal').modal('hide');
                $('#myModal').empty();
            },function(response){
                alert('Erro: '+response.data.message);
                HelperFact.checkTokenValidSession(response.data.message);
            })
        );
    });

    $scope.getRouteFrontEnd();
    $scope.getRoute();

    $("#rotas_api, #rotas_front").select2({
        tags: true
    })
};