/**
 * Created by felipemoura on 23/11/2016.
 */
module.exports = function($scope,$http,$location,appConfig, HelperFact){

    $scope.data = null;
    $scope.produtos = {};
    $scope.usuario = JSON.parse(window.localStorage.getItem('userData'));
    $scope.input_search = null;

    $scope.cadastrarProdutos = function(){
        var format = new Date();
        $scope.data.usuario_id = $scope.usuario.usuario_id;
        $scope.data.seguradora_id = $('#seguradora_id').val();
        $scope.data.produto_criacao = format.getFullYear()+"-"+(format.getMonth()+1)+"-"+format.getDate();
        $scope.data.produto_status = $('#produto_status').val();
        $scope.data.fl_ativo = $('#produto_status').val();

        var token = window.localStorage.getItem('token');
        $http({'method':'post','url':appConfig.host+'/client/produto/store',data: $scope.data, headers:{Authorization: token }})
            .then(function(response){
                    $scope.data = null;
                    alert(response.data.message);
                },
                function(response){
                    var message = response.data.message;
                    if(Array.isArray(message)) {
                        var html = 'Erros: \n';
                        for (i = 0; i < response.data.message.length; i++) {
                            html += response.data.message[i] + '\n';
                        }
                        alert(html);
                    }else {
                        alert(response.data.message);
                        HelperFact.checkTokenValidSession(response.data.message);
                    }
                });
        //regenera o token
        var regenerateToken = HelperFact.getTokenValid();
    };

    $scope.getProdutos = function(){
        var token = window.localStorage.getItem('token');
        var url = appConfig.host+'/client/produto'+($scope.input_search !== null ? "/"+$scope.input_search : "");
        $http({'method':'get','url':url, headers:{Authorization: token }})
            .then(function (response) {
                //console.log(response.data);
                $scope.produtos = response.data;
            },function(response){
                alert(response.data.message);
                //HelperFact.checkTokenValidSession(response.data.message);
            });
        var regenerateToken = HelperFact.getTokenValid();
    };
};