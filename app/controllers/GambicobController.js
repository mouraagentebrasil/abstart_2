/**
 * Created by felipemoura on 23/01/2017.
 */
module.exports = function ($scope, $http, $location, $routeParams, appConfig, HelperFact) {
    $scope.procobs = {};
    $scope.cpf = null;

    $scope.getDataProcob = function(){
        $.blockUI({'message':'Buscando...'});

        $http({
            method: "get",
            url: "https://consultas.agentebrasil.com/getDataProcob/metlife/"+$scope.cpf,
            headers: {"Content-Type": "text/plain"}
        }).then(function (response) {
            if(response.data.error!==undefined){
                $.unblockUI();
                alert(response.data.error);
                $('#tblGCOB').addClass('show_off');
            } else {
                $.unblockUI();
                $('#tblGCOB').removeClass('show_off');
                $scope.procobs = response.data;
                console.log(response.data)
            }
        },function (response) {
            $.unblockUI();
            console.log(response.data);
            alert(response.error)
        });
    };
    //enter no cpf
    $('#cpf').on('keypress',function(e){
        if(e.which==13) {
            $scope.getDataProcob();
        }
    });

};