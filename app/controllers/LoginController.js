/**
 * Created by felipe on 27/06/16.
 */
module.exports = function($scope,$http,$location,appConfig, HelperFact){

    $scope.login = function(user,pass){
        $http({method:"post",data:{login: user,password: pass}, url: appConfig.host+'/client/auth/getTokenByLogin'})
        .then(function(response){
            var data = response.data.usuario;
            data.grupo = response.data.grupo_rota;
            console.log("Logado com sucesso");

            window.localStorage.setItem('token',response.data.token);
            window.localStorage.setItem('userData',JSON.stringify(data));
            window.localStorage.setItem('menus',JSON.stringify(response.data.front_end));
            $scope.infoUser = data;
            $location.path( "/dashboard" );
        },function(response){
            alert('Erro: '+response.data.message);
        });
    };

    $scope.logout = function(){
        $scope.token = window.localStorage.getItem('token');
        $http({
            method:"get",
            headers:{Authorization: "Bearer "+$scope.token},
            url: appConfig.host+'/client/auth/setTokenLogout'
        })
        .then(function(response){
            window.localStorage.removeItem('token');
            window.localStorage.removeItem('userData');
            $location.path("/");
        },function(response){
            window.localStorage.removeItem('token');
            window.localStorage.removeItem('userData');
            $location.path("/");
        });
    }

    $scope.checkToken = function(){
        $scope.token = window.localStorage.getItem('token');
        $http({
            method:"get",
            headers:{Authorization: "Bearer "+$scope.token},
            url: appConfig.host+"/client/auth/getNewTokenByTokenValid"
        }).then(function (response) {
            //console.log(response);
            window.localStorage.setItem('token',response.data.token);
        }, function () {
            $location.path("/");
        });
    }
}