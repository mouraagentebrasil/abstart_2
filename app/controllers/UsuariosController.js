/**
 * Created by felipe on 28/06/16.
 */
module.exports = function($scope,$http,$location,appConfig, HelperFact){

    $scope.data = null;
    $scope.usuarios = null;
    $scope.grupos = null;

    $('#myModal').remove();

    $scope.cadastrarUsuarios = function(data){
        var token = window.localStorage.getItem('token');
        $scope.data.grupo = jQuery('#grupo_usuario').val();
        $http({'method':'post','url':appConfig.host+'/client/usuario/storeWithGrupo',data: $scope.data, headers:{Authorization: token }})
            .then(function(response){
                $scope.data = null;
                alert(response.data.message);
            },
            function(response){
                var html = 'Erros: \n';
                for(i=0;i<response.data.message.length;i++){
                    html += response.data.message[i]+'\n';
                }
                alert(html);
            });
        //regenera o token
        var regenerateToken = HelperFact.getTokenValid();
    };

    $scope.getUsuarios = function(){
        var token = window.localStorage.getItem('token');
        var url = appConfig.host + '/client/usuario';
        $http({method: "get", url: url, headers: {Authorization: "Bearer " + token}})
            .then(function (response) {
                $scope.usuarios = response.data;
                //console.log($scope.usuarios);
            },function (response) {
                alert(response.data.message);
                HelperFact.checkTokenValidSession(response.data.message);
            });
    };

    $scope.modalEditUsuario = function (usuario) {
        $('#myModal').empty();
        html =
            '<div id="myModal" class="modal fade" role="dialog">' +
            '<div class="modal-dialog modal-lg">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
            '<h4 class="modal-title">Atualizar Usuario</h4>' +
            '</div>' +
            '<div class="modal-body">' +
            '<div class="row">' +

            '<div class="form-group">' +
                '<label class="col-sm-2 control-label">Login</label>' +
                '<div class="col-sm-10">' +
                    '<input type="text" class="form-control" id="login" style="width: 400px;" value="'+usuario.login+'">' +
                '</div>' +
            '</div>' +

            '<div class="form-group">' +
                '<label class="col-sm-2 control-label">Email</label>' +
                '<div class="col-sm-10">' +
                    '<input type="text" class="form-control" id="email" style="width: 400px;" value="'+usuario.email+'">' +
                '</div>' +
            '</div>' +

            '<div class="form-group">' +
                '<label class="col-sm-2 control-label">Telefone</label>' +
                '<div class="col-sm-10">' +
                    '<input type="text" class="form-control" id="usuario_telefone" style="width: 400px;" value="'+usuario.usuario_telefone+'">' +
                '</div>' +
            '</div>' +

            '<div class="form-group">' +
                '<label class="col-sm-2 control-label">Senha</label>' +
                '<div class="col-sm-10">' +
                    '<input type="password" class="form-control" id="password" style="width: 400px;">' +
                '</div>' +
            '</div>' +

            '<div class="form-group">' +
                '<label class="col-sm-2 control-label">Status</label>' +
                '<div class="col-sm-10">' +
                    '<select id="fl_ativo" class="form-control" style="width: 400px;">' +
                        '<option value="1" '+(usuario.fl_ativo==1? "selected" : "")+'>Ativo</option>'+
                        '<option value="0" '+(usuario.fl_ativo==0? "selected" : "")+'>Inativo</option>'+
                    '</select>'+
                '</div>' +
            '</div>' +
            $scope.selectGroup(usuario) +
            //$scope.formatRouteFrontend(grupo)+
            '</div>' +
            '</div>'+
            '<input type="hidden" value="'+usuario.usuario_id+'" id="usuario_id">'+
            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>' +
            '<button type="button" class="btn btn-success btn-flat" id="updateUsuario">Atualizar</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        jQuery('.modalAlerts').html(html);
        jQuery('#myModal').modal('toggle');
        $("#rotas_api, #rotas_front").select2({
            tags: true
        });
    };

    $scope.getGrupos = function () {
        var token = window.localStorage.getItem('token');
        $http({method: "get", url: appConfig.host + '/client/grupo', headers: {Authorization: "Bearer " + token}})
            .then(function (response) {
                $scope.grupos = response.data;
            },function(response){
                HelperFact.checkTokenValidSession(response.data.message);
            });
        //regenera o token
        //var regenerateToken = HelperFact.getTokenValid();
    };

    $scope.selectGroup = function(usuario){
        html = '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Grupo de Usuario</label>' +
            '<div class="col-sm-10">' +
            '<select id="grupo_id" class="form-control" style="width: 400px;">';
        for(i=0;i<$scope.grupos.length;i++){
            html+='<option value="'+$scope.grupos[i].grupo_id+'" '+(usuario.grupo_usuario.grupo_id==$scope.grupos[i].grupo_id ? "selected": "")+'>'+$scope.grupos[i].grupo_nome+'</option>';
        }
        html+= '</select>'+
            '</div>' +
            '</div>';
        return html;
    };

    $(document).on('click','#updateUsuario',function(){
        data = [];
        data = {
            'login':$('#login').val(),
            'email':$('#email').val(),
            'fl_ativo':$('#fl_ativo').val(),
            'usuario_telefone':$('#usuario_telefone').val(),
            'usuario_id':$('#usuario_id').val(),
            'grupo_id':$('#grupo_id').val()
        };
        password = $('#password').val();
        if(password!=null){
            data.password = password;
        }
        //usuario/updateWithGrupo
        var token = window.localStorage.getItem('token');
        jQuery.debounce(250,
            $http({'method':'post','url':appConfig.host+'/client/usuario/updateWithGrupo',data: data, headers:{Authorization: token }})
            .then(function(response){
                    alert(response.data.message);
                    $('#myModal').modal('hide');
                    $('#myModal').empty();

                },
                function(response){
                    var html = 'Erros: \n';
                    for(i=0;i<response.data.message.length;i++){
                        html += response.data.message[i]+'\n';
                    }
                    alert(html);
                    HelperFact.checkTokenValidSession(response.data.message);
                })
        );
        //regenera o token
        //var regenerateToken = HelperFact.getTokenValid();
    });

    $scope.getGrupos();
};
