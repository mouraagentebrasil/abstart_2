/**
 * Created by felipemoura on 23/11/2016.
 */
module.exports = function($scope,$http,$location,appConfig, HelperFact){
    $scope.data = null;
    $scope.usuario = JSON.parse(window.localStorage.getItem('userData'));
    $scope.seguradoras = {};

    $scope.cadastrarSeguradora = function () {
        $scope.data.usuario_id = $scope.usuario.usuario_id;
        var token = window.localStorage.getItem('token');
        $http({'method':'post','url':appConfig.host+'/client/seguradora/store',data: $scope.data, headers:{Authorization: token }})
            .then(function(response){
                    $scope.data = null;
                    alert(response.data.message);
                },
                function(response){
                    var html = 'Erros: \n';
                    for(i=0;i<response.data.message.length;i++){
                        html += response.data.message[i]+'\n';
                    }
                    alert(html);
                });
        //regenera o token
        var regenerateToken = HelperFact.getTokenValid();
    };

    $scope.getSeguradoras = function(){
        var token = window.localStorage.getItem('token');
        $http({'method':'get','url':appConfig.host+'/client/seguradora', headers:{Authorization: token }})
            .then(function(response){
                $scope.seguradoras = response.data.data;
            },function(response){
                alert(response.data.message);
            });
        var regenerateToken = HelperFact.getTokenValid();
    };
};
