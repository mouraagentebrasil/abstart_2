module.exports = function ($scope, $http, $location, $routeParams, appConfig, HelperFact) {
    $scope.usuario = JSON.parse(window.localStorage.getItem('userData'));
    $scope.data = null;
    $scope.dateI = null;
    $scope.dateF = null;
    $scope.cobrancas = [];
    $scope.filtro = "todos";
    $scope.filtro_data = "data_vencimento";
    $scope.financeiro_forma_pagamento = "todas";
    $scope.filto_tipo_parcela = "todas";


    $scope.listCobrancas = function(){
        var token = window.localStorage.getItem('token');
        console.log(appConfig.host);
        var url = appConfig.host+'/client/financeiro-ab/getCobrancas/'+$scope.filtro+'/'+$scope.filtro_data+'/'+$scope.financeiro_forma_pagamento+'/'+$scope.filto_tipo_parcela;
        $scope.dateI = $('#dateI').val();
        $scope.dateF = $('#dateF').val();
        url += $scope.dateI == null ? "" : "/"+$scope.dateI;
        url += $scope.dateI == null || $scope.dateI == "" ? "" : "/"+$scope.dateF;
        $.blockUI({'message':"Carregando..."});
        $http({'method':'get', 'url':url, headers:{Authorization: token }})
            .then(function(response){
                $.unblockUI();
                console.log(response);
                $scope.cobrancas = response;
            },function(response){
                $.unblockUI();
                console.log(response);
                HelperFact.checkTokenValidSession(response.data.message);
        });
    };

    $scope.abonarCobranca = function(cobranca_id){
        alert(cobranca_id);
    };

    $scope.gerarCobrancaBoleto = function(cobranca_id){
        alert(cobranca_id);
    };

    $scope.gerarCobrancaCredito = function(cobranca_id){
        alert(cobranca_id);
    };
    $scope.verCob = function(cobranca_id){
        var url = 'http://pag.agentebrasil.com/pagamentos/get-cobranca-by-fcobid/'+cobranca_id;
        $http({'method':'get', 'url':url, headers:{Authorization: token }})
            .then(function(response){
                $.unblockUI();
                console.log(response);
                $scope.cobrancas = response;
            },function(response){
                $.unblockUI();
                console.log(response);
                HelperFact.checkTokenValidSession(response.data.message);
            });
    };
    $scope.gerarCobrancaBoletoCancelamentoAnterior = function (cobranca_id) {
        $scope.newVenc = prompt("Escolha uma nova data de vencimento: ");
        if($scope.newVenc!=null){
            alert('Gerando...'+$scope.newVenc);
        }
    };

    $scope.export = function (items,cob) {

        function isset(object){
            return (typeof object !=='undefined');
        }

        var jsonT =
            'Numero de Pagamentos via boleto: '+cob.qtd_boleto+
            '\nValor Pago via boleto: '+cob.valor_boleto+
            '\nNumero de Pagamentos via Cartao de Credito: '+cob.qtd_cartao+
            '\nValor Pago via Cartao de Credito: '+cob.valor_cartao+'\n\n\n'+
            'Numero de total de pagos: '+cob.total_pagos+
            '\nTotal Arrecadado: '+cob.total_arrecadado+
            '\nValor Total: '+cob.total_geral+'\n\n';


        var jsonObject = [];
        for(cobranca=0;cobranca < items.length; cobranca++){
            try {
                forma_de_pagamento =
                    items[cobranca].financeiro_forma_pagamento_id == 1 ? "Boleto" :
                        items[cobranca].financeiro_forma_pagamento_id == 2 ? "Cartao" : "";
                tipo_pagamento =
                    items[cobranca].fl_primeira_parcela == 1 ? "Primeiro Cobrança" :
                        items[cobranca].fl_primeira_parcela == 0 ? "Recorrência" : "";
                jsonObject[cobranca] = {
                    'cobranca_id(AB)': items[cobranca].financeiro_cobranca_id,
                    'lead_id': isset(items[cobranca].financeiro_contratacao.lead_produto_agregado.lead.lead_id) ? items[cobranca].financeiro_contratacao.lead_produto_agregado.lead.lead_id : "null",
                    'responsavel_financeiro': isset(items[cobranca].financeiro_contratacao.lead_produto_agregado.lead.lead_nome) ? items[cobranca].financeiro_contratacao.lead_produto_agregado.lead.lead_nome : "null",
                    'produto': isset(items[cobranca].financeiro_contratacao.lead_produto_agregado.produto.produto_nome) ? items[cobranca].financeiro_contratacao.lead_produto_agregado.produto.produto_nome : "null",
                    'vencimento': isset(items[cobranca].data_vencimento) ? items[cobranca].data_vencimento : "null",
                    'data_geracao': isset(items[cobranca].data_inclusao) ? items[cobranca].data_inclusao : "null",
                    'valor': isset(items[cobranca].valor) ? items[cobranca].valor.replace('.',',') : "null",
                    'data_pago': isset(items[cobranca].data_pago) ? items[cobranca].data_pago : "null",
                    'valor_pago': isset(items[cobranca].valor_pago) && items[cobranca].valor_pago ? items[cobranca].valor_pago.replace('.',',') : 0,
                    'status': isset(items[cobranca].financeiro_cobranca_status) ? items[cobranca].financeiro_cobranca_status : "null",
                    'vendedor': isset(items[cobranca].usuario.nome) ? items[cobranca].usuario.nome : "null",
                    'seguradora': isset(items[cobranca].financeiro_contratacao.lead_produto_agregado.produto.seguradora.seguradora_nome) ? items[cobranca].financeiro_contratacao.lead_produto_agregado.produto.seguradora.seguradora_nome : "null",
                    'forma_de_pagamento': forma_de_pagamento,
                    'tipo_pagamento': tipo_pagamento,
                    'data_venda':items[cobranca].financeiro_contratacao.data_contratacao,
                    'numero_vidas':items[cobranca].financeiro_contratacao.lead_produto_agregado.lead_produto_agregado_itens.length
                };
            } catch(err) {
                console.log(err);
                console.log(items[cobranca]);
                console.log(items[cobranca].valor_pago);
            }
        }
        JSONToCSVConvertor(jsonObject, jsonT, true);
    };

    function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
        //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
        var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
        var fileName = new Date().getTime();
        var CSV = '';
        //Set Report title in first row or line

        CSV += ReportTitle + '\r\n\n';

        //This condition will generate the Label/Header
        if (ShowLabel) {
            var row = "";

            //This loop will extract the label from 1st index of on array
            for (var index in arrData[0]) {

                //Now convert each value to string and comma-seprated
                row += index + ';';
            }

            row = row.slice(0, -1);
            //append Label row with line break
            CSV += row + '\r\n';
        }

        //1st loop is to extract each row
        for (var i = 0; i < arrData.length; i++) {
            var row = "";

            //2nd loop will extract each column and convert it in string comma-seprated
            for (var index in arrData[i]) {
                row += arrData[i][index] + ';';
            }

            row.slice(0, row.length - 1);

            //add a line break after each row
            CSV += row + '\r\n';
        }

        if (CSV == '') {
            alert("Invalid data");
            return;
        }

        //Generate a file name
        //var fileName = "Report_";
        //this will remove the blank-spaces from the title and replace it with an underscore
        //fileName += ReportTitle.replace(/ /g,"_");

        //Initialize file format you want csv or xls
        var uri = 'data:text/csv,' + escape(CSV);

        // Now the little tricky part.
        // you can use either>> window.open(uri);
        // but this will not work in some browsers
        // or you will not get the correct file extension

        //this trick will generate a temp <a /> tag
        var link = document.createElement("a");
        link.href = uri;

        //set the visibility hidden so it will not effect on your web-layout
        link.style = "visibility:hidden";
        link.download = fileName + ".csv";

        //this part will append the anchor tag and remove it after automatic click
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }

    $('#dateI').datepicker({
        "useCurrent": true,
        "setDate": new Date(),
        "autoclose": true,
        "language": 'pt',
        "format": "yyyy-mm-dd"
    });

    $('#dateF').datepicker({
        "useCurrent": true,
        "setDate": new Date(),
        "autoclose": true,
        "language": 'pt',
        "format": "yyyy-mm-dd"
    });
};