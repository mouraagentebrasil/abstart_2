/**
 * Created by felipe on 29/06/16.
 */
module.exports = function($routeProvider,$locationProvider){
    $routeProvider
        .when("/",{
            templateUrl: "views/login.html"
        })
        .when("/login",{
            templateUrl: "views/login.html"
        })
        .when("/dashboard",{
            templateUrl: "views/dashboard.html"
        })//grupo
        .when("/grupo/cadastrar",{
            templateUrl: "views/grupo/cadastro.html"
        })
        .when("/grupo/listar",{
            templateUrl: "views/grupo/listar.html"
        })
        .when("/grupo/cadastrar/rota",{
            templateUrl: "views/dashboard.html"
        })//produto
        .when("/produto/cadastro",{
            templateUrl: "views/produto/cadastro.html"
        })
        .when("/produto/listar",{
            templateUrl: "views/produto/listar.html"
        })//campanha
        .when("/campanha/cadastro",{
            templateUrl: "views/campanha/cadastro.html"
        })
        .when("/campanha/listar",{
            templateUrl: "views/campanha/listar.html"
        })//seguradora
        .when("/seguradora/cadastro",{
            templateUrl: "views/seguradora/cadastro.html"
        })
        .when("/seguradora/listar",{
            templateUrl: "views/seguradora/listar.html"
        })//usuario
        .when("/usuario/cadastrar",{
            templateUrl: "views/usuario/cadastro.html"
        })
        .when("/usuario/listar",{
            templateUrl: "views/usuario/listar.html"
        })
        //lead
        .when("/lead/cadastro",{
            templateUrl: "views/lead/cadastro.html"
        })
        .when("/lead/operacoes",{
            templateUrl: "views/lead/operacoes.html"
        })
        .when("/lead/operacao/agregar/:lead_id",{
            templateUrl: "views/lead/operacao_agregar.html"
        })
        .when("/financeiro/dashboard",{
            templateUrl: "views/financeiro/dashboard_financeiro.html"
        })
        .when("/rede-credenciada/consultas",{
            templateUrl: "views/rede-credenciada/consultas.html"
        })
        .when("/rede-credenciada/estatiscas",{
            templateUrl: "views/rede-credenciada/estatiscas.html"
        })
        .when("/procob/consultas",{
            templateUrl: "views/gambicob/dashboard_gambicob.html"
        })
    ;
};
